mac=(navigator.appVersion.indexOf('Mac')!=-1)?true:false;
ie=(navigator.appName.charAt(0)=="M")?true:false;
document.write("<STYLE TYPE='text/css'><!--");
if(mac){

	if(ie){		//Macintosh版IE用の記述
		document.writeln(".t1s{color: #333333; font-size: 0.74em; line-height: 1.3em;}");
		document.writeln(".t1{color: #333333; font-size: 0.8em; line-height: 1.5em;}");
		document.writeln(".t2{color: #333333; font-size: 0.9em ;line-height: 1.5em;}");
		document.writeln(".t3{color: #333333; font-size: 1.0em ;line-height: 1.3em;}");
		document.writeln(".t1b{color: #333333; font-size: 0.8em; line-height: 1.5em; font-weight: bold}");
		document.writeln(".t2b{color: #333333; font-size: 0.9em ;line-height: 1.5em; font-weight: bold}");
		document.writeln(".t3b{color: #333333; font-size: 1.0em ;line-height: 1.3em; font-weight: bold}");
		document.writeln(".t1l{color: #333333; font-size: 0.8em; line-height: 1.5em; text-decoration:underline;}");
	}
	else{
		//Macintosh版Neとその他用の記述
		document.writeln(".t1s{color: #333333; font-size: 0.8em; line-height: 1.3em;}");
		document.writeln(".t1{color: #333333; font-size: 0.82em; line-height: 1.3em;}");
		document.writeln(".t2{color: #333333; font-size: 0.9em; line-height: 1.5em;}");
		document.writeln(".t3{color: #333333; font-size: 1.0em; line-height: 1.3em;}");
		document.writeln(".t1b{color: #333333; font-size: 0.82em; line-height: 1.5em; font-weight: bold}");
		document.writeln(".t2b{color: #333333; font-size: 0.9em; line-height: 1.5em; font-weight: bold}");
		document.writeln(".t3b{color: #333333; font-size: 1.0em; line-height: 1.3em; font-weight: bold}");
		document.writeln(".t1l{color: #333333; font-size: 0.82em; line-height: 1.5em; text-decoration:underline;}");

	}
}
else{
		//Win版IE用の記述
	if(ie){
		document.writeln(".t1s{color: #333333; font-size: 0.7em; line-height: 1.3em;}");
		document.writeln(".t1{color: #333333; font-size: 0.8em; line-height: 1.5em;}");
		document.writeln(".t2{color: #333333; font-size: 0.9em; line-height: 1.5em;}");
		document.writeln(".t3{color: #333333; font-size: 1.0em; line-height: 1.7em;}");
		document.writeln(".t1b{color: #333333; font-size: 0.8em; line-height: 1.5em; font-weight: bold}");
		document.writeln(".t2b{color: #333333; font-size: 0.9em; line-height: 1.5em; font-weight: bold}");
		document.writeln(".t3b{color: #333333; font-size: 1.0em; line-height: 1.7em; font-weight: bold}");
		document.writeln(".t1l{color: #333333; font-size: 0.8em; line-height: 1.5em; text-decoration:underline;}");
	}
	else{
		//Win版Neとその他の用の記述
		document.writeln(".t1s{color: #333333; font-size: 0.9em; line-height: 1.3em;}");
		document.writeln(".t1{color: #333333; font-size: 1.0em; line-height: 1.5em;}");
		document.writeln(".t2{color: #333333; font-size: 1.1em; line-height: 1.5em;}");
		document.writeln(".t3{color: #333333; font-size: 1.2em; line-height: 1.6em;}");
		document.writeln(".t1b{color: #333333; font-size: 1.0em; line-height: 1.5em; font-weight: bold}");
		document.writeln(".t2b{color: #333333; font-size: 1.1em; line-height: 1.5em; font-weight: bold}");
		document.writeln(".t3b{color: #333333; font-size: 1.2em; line-height: 1.6em; font-weight: bold}");
		document.writeln(".t1l{color: #333333; font-size: 1.0em; line-height: 1.5em; text-decoration:underline;}");
	}	
}

document.write("--></STYLE>");