// ----------------------------------------------------------------------------
// SP QA List Accordion
// ----------------------------------------------------------------------------
$(function(){
    var SPQAListAccordion  = function(){
        $('.list-qa-01').each(function(i){
    		var $question = $(this).find('.question');

    		$question.on("click", function(e){
    			e.preventDefault();
    			var $this = $(this);
    			$this.toggleClass('active');
    			$this.next('.answer').toggleClass('active');
    		});
    	});
    }
    SPQAListAccordion(); 
});
