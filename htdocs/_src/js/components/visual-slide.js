// ----------------------------------------------------------------------------
// Top Visual Slide
// ----------------------------------------------------------------------------
$(function(){
    TopVisualSlideShow = function(){
        var options = {
            easing   : 'swing',
            interval : 5000
        };

        var $window = $(window);
        var $windowWidth = $window.width();
        var $visualSlide = $('.lyt-main-visual-01');
        var $visualSlideInner = $('.lyt-main-visual-01-inner');
        var $visualList = $('.visual-list');
        var $visualListLiLen = $('.visual-list').find('li').length;
        var $thumbnailListContainer = $('.thumbnail-list-container-01');
        var $thumbnailList = $('.thumbnail-list-container-01').find('ul');
        var $thumbnailListLi = $thumbnailList.find('li');
        var $thumbnailListLiWidth = 0;
        var $setHeightImg = $visualSlide.find('.setheight-img');
        var $thumbnailListLiMargin = parseInt( $thumbnailList.find('li').css('marginRight') );
        var $thumbnailListSP = $('.thumbnail-list-sp');
        var $next = $thumbnailListContainer.find('.next');
        var $prev = $thumbnailListContainer.find('.prev');
        var posNum = 1;
        var flag = true;
        var $defaultThumbnailList = $thumbnailList.html();
        var $defaultThumbnailListLen = $thumbnailList.find('li').length;
        var $tbLi01 = $thumbnailList.find('li').eq(0).clone();
		var	$tbLi02 = $thumbnailList.find('li').eq(1).clone();
        var	$tbLi03 = $thumbnailList.find('li').eq(2).clone();
        var	$tbLi04 = $thumbnailList.find('li').eq(3).clone();
		var	$tbLiB01 = $thumbnailList.find('li').eq( $defaultThumbnailListLen -1 ).clone();
		var	$tbLiB02 = $thumbnailList.find('li').eq( $defaultThumbnailListLen -2 ).clone();
		var	$tbLiB03 = $thumbnailList.find('li').eq( $defaultThumbnailListLen -3 ).clone();
        var	$tbLiB04 = $thumbnailList.find('li').eq( $defaultThumbnailListLen -4 ).clone();

        if( $defaultThumbnailListLen > 3 ){
            $thumbnailList.append( $tbLi01 ).append( $tbLi02 ).append( $tbLi03 ).append( $tbLi04 ).prepend( $tbLiB01 ).prepend( $tbLiB02 ).prepend( $tbLiB03 ).prepend( $tbLiB04 );
        }

		var $addThumbnailListLi = $thumbnailList.find('li');
		var $addThumbnailListLiLen = $addThumbnailListLi.length;

        var initialize = function(){
            var imgs = [];

            $visualList.find('img').each(function(){
                var imgSrc = $(this).attr('src');
                imgs.push(imgSrc);
            });

            pagerSetSP();

            imagesLoaded( $visualSlide, function() {
                windowResize();

                $('.loader').fadeOut(1000, function(){
                    $thumbnailListLiWidth = $thumbnailList.find('li').outerWidth();

                    if( $defaultThumbnailListLen > 3 ){
                        $thumbnailList.css({ 'margin-left' : -( ($thumbnailListLiWidth + $thumbnailListLiMargin) * 4 )});
                        $addThumbnailListLi.eq(4).addClass('current');
                    } else{
                        $thumbnailListContainer.find('.thumbnail-list-inner').css({ 'margin-left' : 0 });
                        $prev.hide();
                        $next.hide();
                        $addThumbnailListLi.eq(0).addClass('current');
                    }

                    $thumbnailList.css({ 'width' : ($thumbnailListLiWidth + $thumbnailListLiMargin) * $addThumbnailListLiLen });

                    $visualList.find('li').css({ 'opacity' : '0', 'z-index' : 0 });
                    $visualList.css("visibility","visible");
                    $visualList.find('li:first-child').addClass('current').stop().animate({ 'opacity' : 1, 'z-index' : 1 },1000, options.easing);
                    $thumbnailListContainer.stop().animate({ 'opacity' : 1 },1000, options.easing);
                    timer = setInterval(switchImg, options.interval);

                    setTimeout(function(){
                        $visualSlideInner.css({ 'background-color' : '#fff' });
                    }, 5000);
                });
            });
        };


        var pagerSetSP = function(){
			for( var i = 0; i < $defaultThumbnailListLen; i++ ){
				$thumbnailListSP.append('<li></li>');
			}

            $pagerLi = $thumbnailListSP.find('li');
            $pagerLi.eq(0).addClass('current');

			$pagerLi.on('click', function(){
				if(! $(this).hasClass('current') ){
					clearInterval(timer);
					posNum = $pagerLi.index(this);
					switchImg();
				}
			});
		}

        var windowResize = function(){
            $(window).resize(function(){
                setTimeout(function(){
                    if( Window.viewport().height < $('#header').height() + $('.lyt-contact-top-01').height() + $setHeightImg.height() && Window.viewport().width > 1064 && Window.viewport().height > 700 ){
                        $visualSlideInner.height( Window.viewport().height - $('#header').height() - $('.lyt-contact-top-01').height() );
                        var $liMt = ($visualSlideInner.find('.text').height() - $visualSlideInner.height())/2;
                        $visualSlideInner.find('.text').css({ 'margin-top' : - $liMt });
                    } else{
                        $visualSlideInner.height( $setHeightImg.height() );
                        $visualSlideInner.find('.text').css({ 'margin-top' : 0 });
                    }
                    var $imgMt = ($visualList.find('.image img').height() -  $visualSlideInner.height()) / 2;
                    $visualList.find('.image').css({ 'margin-top' : - $imgMt });
                }, 100);
            }).trigger('resize');
        };


        $addThumbnailListLi.on('click', function(){
            if(! $(this).hasClass('current') && flag === true ){
                clearInterval(timer);
                posNum = $addThumbnailListLi.index(this) - 4;

                if( $defaultThumbnailListLen > 3 ){
                    posNum = $addThumbnailListLi.index(this) - 4;
                } else{
                    posNum = $addThumbnailListLi.index(this);
                }

                switchImg();
            }
        });


        $next.on('click', function(){
            if( flag === true ){
                clearInterval(timer);
                switchImg();
            }
		});


        $prev.on('click', function(){
            if( flag === true ){
                clearInterval(timer);
                posNum = posNum - 2;
                switchImg();
            }
		});


        $visualSlide.touchwipe({
			wipeLeft: function(){
				$next.click(); 
			},

			wipeRight: function(){
				$prev.click();
			},

			preventDefaultEvents: false
		});


        var switchImg = function(){
            if( flag === true ){
                flag = false;

                if( $defaultThumbnailListLen == posNum){
                    $thumbnailList.find('.current').removeClass('current');

                    if( $defaultThumbnailListLen > 3 ){
                        $thumbnailList.find('li').eq(posNum+4).addClass('current');
                        $thumbnailList.find('li').eq(4).addClass('current');
                    } else{
                        $thumbnailList.find('li').eq(0).addClass('current');
                    }

                    $visualList.find('li').eq(0).stop().animate({ 'opacity' : 1 },300, options.easing);
                    $thumbnailListSP.find('.current').removeClass('current');
                    $pagerLi.eq(0).addClass('current');

                    $visualList.find('.current').stop().animate({ opacity : 0 }, 500, options.easing, function(){
                        $visualList.find('.current').css({ 'z-index' : 0 }).removeClass('current');
                        $visualList.find('li').eq(0).css({ 'z-index' : 1 }).addClass('current');
                    });
                } else{
                    $thumbnailList.find('.current').removeClass('current');

                    if( $defaultThumbnailListLen > 3 ){
                        $thumbnailList.find('li').eq(posNum+4).addClass('current');
                    } else{
                        $thumbnailList.find('li').eq(posNum).addClass('current');
                    }

                    $thumbnailListSP.find('.current').removeClass('current');
                    $pagerLi.eq(posNum).addClass('current');

                    $visualList.find('.current').stop().animate({ opacity : 0 }, 500, options.easing, function(){
                        $(this).removeClass('current');
                    });

                    if( posNum == $defaultThumbnailListLen ){
                        $thumbnailList.find('li').eq(5).addClass('current');
                        $visualList.find('li').eq(1).addClass('current').animate({ 'opacity' : 1 },300, options.easing);
                    } else if( posNum == - 4 ){
                        $thumbnailList.find('li').eq($defaultThumbnailListLen).addClass('current');
                        $visualList.find('li').eq($defaultThumbnailListLen - 4).addClass('current').animate({ 'opacity' : 1 },300, options.easing);
                    } else if( posNum == $defaultThumbnailListLen + 1 ){
                        $thumbnailList.find('li').eq(5).addClass('current');
                        $visualList.find('li').eq(1).addClass('current').animate({ 'opacity' : 1 },300, options.easing);
                    } else{
                        $visualList.find('li').eq(posNum).addClass('current').animate({ 'opacity' : 1 },300, options.easing);
                    }
                }

                if( $defaultThumbnailListLen > 3 ){
                    $thumbnailList.animate({ 'marginLeft': -(($thumbnailListLiWidth + $thumbnailListLiMargin) * posNum) - (($thumbnailListLiWidth + $thumbnailListLiMargin) * 4) }, options.duration, options.easing, callback);
                } else{
                    callback();
                }
            }

            function callback(){
                clearInterval(timer);

                if( posNum == $defaultThumbnailListLen ){
                    if( $defaultThumbnailListLen > 3 ){
                        $thumbnailList.css({ 'margin-left' :  -( ($thumbnailListLiWidth + $thumbnailListLiMargin) * 4 ) });
                    }
                    $thumbnailList.find('li').eq(posNum+4).removeClass('current');
                    posNum = 1;
                } else if( posNum == - 4 ){
                    $thumbnailList.css({ 'margin-left' :  -( ($thumbnailListLiWidth + $thumbnailListLiMargin) * $defaultThumbnailListLen ) });
                    posNum = $defaultThumbnailListLen - 3;
                } else if( posNum == $defaultThumbnailListLen + 1 ){
                    $thumbnailList.css({ 'margin-left' :  -( ($thumbnailListLiWidth + $thumbnailListLiMargin) * 5 ) });
                    posNum = 2;
                } else{
                    posNum++;
                }

                setTimeout(function(){
                    flag = true;
                }, 500);
                timer = setInterval(switchImg, options.interval);
            }
        }

        initialize();
    }

    if( $('.lyt-main-visual-01').length > 0 ){
        TopVisualSlideShow();
    }
});
