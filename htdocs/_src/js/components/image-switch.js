// ----------------------------------------------------------------------------
// IMG Switch
// ----------------------------------------------------------------------------
$(function(){
    var ImgSwitch = function(){
        var SPWidth = 760,
            TBWidth = 1064,
            $window = $(window);

        var onResize = function(){
            var imgSwitchSP = function(options){
                var classname  = options.classname;

                if( Window.viewport().width <= SPWidth ){
                    $(classname).each(function(){
                        $(this).attr("src",$(this).attr("src").replace('_pc', '_sp'));
                    });
                } else{
                    $(classname).each(function(){
                        $(this).attr("src",$(this).attr("src").replace('_sp', '_pc'));
                    });
                }
            };

            var imgSwitchTB = function(options){
                var classname  = options.classname;

                if( Window.viewport().width <= TBWidth ){
                    $(classname).each(function(){
                        $(this).attr("src",$(this).attr("src").replace('_pc', '_sp'));
                    });
                } else{
                    $(classname).each(function(){
                        $(this).attr("src",$(this).attr("src").replace('_sp', '_pc'));
                    });
                }
            };
            imgSwitchSP({ classname:'.js-img-switch-sp' });
            imgSwitchTB({ classname:'.js-img-switch-tb' });
        }

        $window.on('resize', function(){
            if( ua.uaObj.uaBrouser !== 'ie8' ){
                onResize();
            }
        }).trigger('resize');
    }
    ImgSwitch();
});
