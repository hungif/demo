// ----------------------------------------------------------------------------
// Page Top
// ----------------------------------------------------------------------------
$(function(){
    var pageTop = function() {
        var y,
            num=50,
            $el=$('#nav-page-top-01');

        function scroll() {
            $(window).on('scroll', function() {
                y = window.pageYOffset;
                onload();
            });
        }

        function onload() {
            if(num<y){
                $el.stop().fadeIn(300);
                scrollAction();
            } else{
                $el.stop().fadeOut(100);
            }
        }

        function scrollAction() {
            $el.on('click', function() {
                $('html,body').stop().animate({'scrollTop':0},500);
            });
        }

        return{
            start: scroll
        };
    };
    if ( ua.uaObj.uaDevice !== 'device-mobile' && $('#nav-page-top-01').size() > 0 ) {
        var module = pageTop();
        module.start();
    } 
});