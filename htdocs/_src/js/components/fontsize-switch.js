// ----------------------------------------------------------------------------
// Font Size Switch
// ----------------------------------------------------------------------------
$(function(){
    var fontsizeSwitch = function (){
        var changeBtn = $("#fs-switch").find(".change-btn"),
            fontSize = [100,112,124],
            sizeLen = fontSize.length,
            activeClass = "active",
            ovStr = "_o",
            defaultSize = 0;

        function nowCookie(){
            return $.cookie("fontsize");
        }

        function cookieSet(index){
            $.cookie("fontsize",fontSize[index],{path:'/',expires:7});
        }

        function sizeChange(){
            $("#content").css({ fontSize : nowCookie()+"%" });
        }

        changeBtn.click(function(){
            var $this = $(this);
            var index = changeBtn.index(this);
            $this.parent().addClass('current');
            cookieSet(index);
            sizeChange();
            $('#fs-switch').find('li').removeClass('current');
            $(this).parent().addClass('current');
        });

        if(nowCookie()){
            for(var i=0; i<sizeLen; i++){
                if(nowCookie()==fontSize[i]){
                    sizeChange();
                    var elm = changeBtn.eq(i);
                    changeBtn.eq(i).parent().addClass('current');
                    break;
                }
            }
        }
        else {
            $('#font-S').addClass('current');
            cookieSet(defaultSize);
            sizeChange();
            var elm = changeBtn.eq(defaultSize);
        }
    }
    fontsizeSwitch();
});
