// ----------------------------------------------------------------------------
// Modal Window
// ----------------------------------------------------------------------------
$(function(){
    var modalWindow = function(){
        var $window = $(window),
            $overlay = $('#overlay'),
            $modalBtn = $('.js-modal'),
            $modalBox = '',
            $modalCont = {},
            modalBoxPosTop = 0,
            windonwHeight = 0,
            modalHeight = 0;

        var options = {
            duration : 300,
            easing   : 'swing'
        };

        var init = function(){
            eventStart();
        };
        var eventStart = function() {
            $modalBtn.on('click', function(e){
                e.preventDefault();
                $(this).closest('li,p').addClass('current');
                $modalCont ={
                    'title': $(this).closest('.column').find('.ad-title').text(),
                    'new'  : $(this).closest('.column').find('.new').html() !== undefined ? $(this).closest('.column').find('.new').text() : ' ',
                    'nav'  : $(this).closest('.nav-btn-02').clone().addClass('center')
                };
                $modalBox = $(this).attr('href');

                // liをカウント
                var arr = $(this).closest('.nav-btn-02').find('li,p');
                if(arr.length < 2) $modalCont.nav.addClass('modal-nav-01');
                // classの付け替え
                if($modalCont.nav.hasClass('w-100')) $modalCont.nav.removeClass('w-100').addClass('w-121');

                // modalコンテンツをセット
                $($modalBox).find('.content').append($modalCont.nav);
                $($modalBox).find('.ad-new').text($modalCont.new);
                $($modalBox).find('.ad-title').text($modalCont.title);
                $(this).closest('li,p').removeClass('current');
                eventSet($(this));
            });
            $('#overlay, .modal-close').on('click', function(){
                modalClose();
            });

            $window.on('resize', function(){
                if ( ua.uaObj.uaDevice != 'device-mobile' ) {
                    modalClose();
                }
            });
        };

        var eventSet = function($this){
            $modalCont.url = $this.hasClass('movie') ? '<iframe src="'+$this.data('url')+'" allowfullscreen></iframe>' : $this.data('url');
            modalBoxPosTop = $window.scrollTop();

            // modalコンテンツをセット
            $($modalBox).find('.movie-box').html($modalCont.url);

            if(!$($modalBox).hasClass('open')){
                $($modalBox).imagesLoaded( function() {
                    modalOpen();
                });
            }
        };

        var modalOpen = function(){
            $overlay.height( $(document).height() ).fadeIn(options.duration, options.easing);

            windonwHeight = $window.height(),
            modalHeight = $($modalBox).height();

            modalTopMargin = ( windonwHeight - modalHeight + 15 ) / 2;

            var modalTopM = modalBoxPosTop + modalTopMargin;
                modalLeft = $($modalBox).outerWidth()/2;

            $($modalBox).show();
            $($modalBox).css({ 'top' : modalTopM, 'marginLeft' : -modalLeft });

            setTimeout(function(){
                $($modalBox).addClass('open');
                // modal内のコンテンツを変更
                $($modalBox).find('a').on('click', function(e) {
                    e.preventDefault();
                    $($modalBox).find('li').removeClass('current');
                    $(this).closest('li').addClass('current');
                    eventSet($(this));
                });
            }, 200);
        }

        var modalClose = function(){
            $overlay.fadeOut(options.duration, options.easing);
            $($modalBox).removeClass('open');
            setTimeout(function(){
                $($modalBox).hide();
                $($modalBox).css({ 'top' : 0 } );
                $($modalBox).find('.nav-btn-02').remove();
                $($modalBox).find('.movie-box').html('');
            }, 500);
        }
        init();
    }
    modalWindow();
});
