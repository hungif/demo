
$(function(){
    var headerNavStay = function(){
		var path = location.pathname.split('/');
		var dir2nd = path[2];
        var dir3rd = path[3].split('.')[0];
		var $navGlobal = $('#nav-global');
        var $navCategory = $('#nav-category');
		var $navUtility = $('#nav-utility');

        if( dir2nd ){
            $navCategory.find('.'+ dir2nd).addClass('current');
            $navUtility.find('.'+ dir2nd).addClass('current');
        }

        if( dir3rd ){
            $navGlobal.find('.'+ dir3rd).addClass('current');

            if( dir3rd == 'contact' ){
                $navUtility.find('.contact').addClass('current');
            }
        }
	};
	headerNavStay();

    var formShowPC = function(){
        var $siteSearch = $('#site-search');
        var $input = $siteSearch.find('.input-text');
        var width = 1000;
        var flag = true;

        $siteSearch.on({
            'mouseenter':function(){
                if( Window.viewport().width <= width ){
                    $input.addClass('open');
                }
            },
            'mouseleave':function(){
                if( Window.viewport().width <= width ){
                    $input.removeClass('open');
                }
            }
        });
    }
    formShowPC();


    var formShow = function(){
        var $siteSearch = $('#site-search');
        var $btnOpen = $('#btn-site-search');
        var $btnClose = $('#btn-site-search-close');
        var flag = true;
        var width = 1064;

        $btnOpen.on('click', function(){
            $btnClose.show();
            $siteSearch.show();
        });

        $btnClose.on('click', function(){
            $(this).hide();
            $siteSearch.hide();
        });

        var windowResize = function(){
            if( Window.viewport().width > width ){
                $siteSearch.show();
                $btnClose.hide();
            } else{
                $siteSearch.hide();
                $btnClose.hide();
            }
        }

        $(window).on('resize', function(){
            if( ua.uaObj.uaDevice !== 'device-mobile' ){
                windowResize();
            }
        });
    }
    formShow();


    var InputBoxHover = function(){
        var $inputBox = $('#site-search .input-text');
        var $inputBoxFooter = $('#site-search-footer .input-text');
        var width = 1064;

        $inputBox.on('focus', function(){
            if( Window.viewport().width > width ){
                $(this).css({ backgroundColor: '#fff', border:'1px solid #d1ced7' });
            }
        }).on('blur', function(){
            if( Window.viewport().width > width ){
                $(this).css({ backgroundColor: '#eaeaf0', border:'none' });
            }
        });

        $inputBoxFooter.on('focus', function(){
            if( Window.viewport().width > 760 ){
                $(this).parent().find('.search-img').attr("src",'/assets/img/icon/icon_search_02.png');
            }
        }).on('blur', function(){
            if( Window.viewport().width > 760 ){
                $(this).parent().find('.search-img').attr("src",'/assets/img/icon/icon_search_01.png');
            }
        });

        var windowResize = function(){
            if( Window.viewport().width > width ){
                $inputBox.css({ backgroundColor: '#eaeaf0', border:'none' });
            } else{
                $inputBox.css({ backgroundColor: '#fff', border:'none' });
            }
        }

        $(window).on('resize', function(){
            windowResize();
        });
    };
    InputBoxHover();


    var navGlobalSP = function(){
        var $hamburger = $('#btn-hamburger');
        var $globalNav = $('#nav-global-sp');
        var $globalNavWidth = 85.9375;
        var $globalNavInner = $('#nav-global-sp-inner');
        var $globalNavTitle = $globalNav.find('.title-01');
        var $hand = $globalNav.find('.hand');
        var $navGlobalSpClose = $('#nav-global-sp-close');
        var $langAcdBtn = $('#nav-global-sp').find('.lang-container');
        var flag = true;

        $hamburger.on('click', function(){
            if( flag === true ){
                flag = false;
                $hand.addClass('moving');
                $( "body" ).addClass('fixed').css({ height : Window.viewport().height });
                $( "body" ).append( '<div id="nav-global-sp-overlay"></div>' );
                $('#nav-global-sp-overlay').fadeIn(200);
                TweenMax.to($globalNav, 0.3, {
                    left: '0',
                    width: '92%',
                    ease: Power1.easeOut,
                    onComplete: function(){
                        TweenMax.to($globalNav, 0.2, {
                            width: $globalNavWidth + '%',
                            ease: Power1.easeOut,
                            onComplete: function(){
                                $( "#header" ).append( '<p id="nav-global-sp-close"><span></span></p>' );
                                $('#nav-global-sp-close').show();
                                navGlobalSPClose();
                                flag = true;
                            }
                        });
                    }
                });
                $(window).on('orientationchange', function(){
                    navGlobalSPCloseApp($('#nav-global-sp-close'));
                });
            }
        });

        var navGlobalSPClose = function(){
            $('#nav-global-sp-close').on('click', function(){
                if( flag === true ){
                    flag = false;
                    navGlobalSPCloseApp($(this));
                }
            });
        };

        var navGlobalSPCloseApp = function($this) {
            $( "body" ).removeClass('fixed').css({ height : 'auto' });
            $hand.addClass('moving');
            $this.hide();
            $this.fadeOut(200, function(){
                $('#nav-global-sp-close').remove();
            });
            $('#nav-global-sp-overlay').fadeOut(200, function(){
                $('#nav-global-sp-overlay').remove();
                $('#header').removeAttr('style').css({'top':0});
            });
            TweenMax.to($globalNav, 0.4, {
                left: '-100%',
                ease: Back.easeOut.config(1.4),
                onComplete: function(){
                    $hand.removeClass('moving');
                    flag = true;
                }
            });
        };

        var windowResize = function(){
            $globalNavInner.height(Window.viewport().height - $globalNavTitle.outerHeight());
            if( $globalNavTitle.outerHeight() + $globalNavInner.find('nav').height() >= Window.viewport().height ){
                $globalNavInner.addClass('scroll');
            } else{
                $globalNavInner.removeClass('scroll');
            }
        }

        $langAcdBtn.on("click", function(e){
            e.preventDefault();
            $(this).parent().toggleClass('active');
            windowResize();
        });

        $(window).on('resize', function(){
            windowResize();
        }).trigger('resize');
    }
    navGlobalSP();


    var switchModalWindow = function(){
        var $window = $(window),
            $modalBtn = $('.js-switch-modal'),
            $modalBox = '',
            modalBoxPosTop = 0,
            windonwHeight = 0,
            modalHeight = 0;
            flag = true;

        var options = {
            duration : 500,
            easing   : 'linear'
        };

        var init = function(){
            $( "body" ).append( '<div id="switch-overlay"></div>' );
            eventSet();
        }

        var eventSet = function(){
            $modalBtn.on('click', function(e){
                if( flag === true ){
                    flag = false;
                    e.preventDefault();
                    $modalBox = $(this).attr('href'),
                    modalBoxPosTop = $window.scrollTop();
                    modalOpen();
                }
            });


            $('#switch-overlay, .switch-modal .btn-close, .btn-submit').on('click', function(){
                modalClose();
            });
        }

        var modalOpen = function(){
            $('#switch-overlay').fadeIn(200);
            windonwHeight = $window.height(),
            modalHeight = $($modalBox).height();
            modalTopMargin = ( windonwHeight - modalHeight ) / 2;
            var modalTopM = modalTopMargin;
            modalLeft = $($modalBox).outerWidth()/2;
            $($modalBox).show();
            $($modalBox).css({ 'top' : modalTopM, 'marginLeft' : -modalLeft });
            setTimeout(function(){
                $($modalBox).addClass('open');
            }, 200);
        }

        var modalClose = function(){
            $('#switch-overlay').fadeOut(options.duration, options.easing);
            $($modalBox).removeClass('open');
            setTimeout(function(){
                $($modalBox).hide();
                $($modalBox).css({ 'top' : 0 } );
                flag = true;
            }, 300);
        }

        init();
    }
    switchModalWindow();


    var spHeaderFix = function(){
        var scrH    = 0,
            docH    = 0,
            footrH  = 0,
            flg     = true,
            headerH = $('#header').outerHeight(),
            header  = $('#header');

        function init(){
            $(window).on('resize load', function(){
                if(  $(window).width() < 836) start();
            });
        }

        function start(num){
            $(window).on('scroll', function() {
                scrH = $(document).scrollTop();
                winH = window.innerHeight;
                docH = $(document).height();
                    if(docH - winH === scrH){
                        if(flg === true){
                            header.addClass('fixed').animate({'top':'0px'},500);
                            $('#container').addClass('sp-fixed-header');
                            flg  = false;
                        }
                    }else if(docH - headerH - winH > scrH){
                        if(flg === false){
                            header.stop().animate({'top':'-60px'},500, function(){
                                $('#container').removeClass('sp-fixed-header');
                                $(this).removeClass('fixed');
                            });
                            flg = true;
                        }
                    }
            });
            $('#btn-hamburger').on('click', function() {
                header.css({'position':'static'});
            });
        }
        init();
    };
    spHeaderFix();
});
