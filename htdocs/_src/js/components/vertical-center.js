// ----------------------------------------------------------------------------
// Vertical Center
// ----------------------------------------------------------------------------
$(function(){
    var verticalCenter01 = function(){
		$(window).load(function (){
			var txtMarginSet = function(){
				$('.v-center-01, .v-center-02').each(function(){
					var $this      = $(this),
						$img       = $(this).find('.image'),
						$text      = $(this).find('.text'),
						$textInner = $(this).find('.text-inner');

					$text.height( $img.height() );
					$textInner.css({ 'margin-top' : - ( $($textInner).height() / 2 ) }).css("visibility","visible");
				});
			}
			txtMarginSet();

			$(window).resize(function(){
				txtMarginSet();
			});
		});
	};
	verticalCenter01();
});
