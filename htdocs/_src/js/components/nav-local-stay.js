
$(function(){
    var NavLocalStay = function(){
        var path = location.pathname.split('/');
        var dir2nd = path[2];
        var dir3rd = path[3].split('.')[0];
        var $navLocal = $('.nav-local-01');

        $navLocal.find('.'+ dir3rd).addClass('current');
    }

    if( $('.nav-local-01').length > 0 ){
        NavLocalStay();
    }
});
