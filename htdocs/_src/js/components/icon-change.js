// ----------------------------------------------------------------------------
// iconDateChange
// ----------------------------------------------------------------------------
$(function(){
    var iconDateChange = function(){
        var date = new Date();
        var hours = date.getHours();

        if( hours > 6 && hours < 18 ){ //afternoon 06:00 ~ 17:59
            $('.js-icon-change').addClass('auto-noon');
        } else{ // night 18:00 ~ 05:59
            $('.js-icon-change').addClass('auto-night');
        }
    }
    iconDateChange();
});
