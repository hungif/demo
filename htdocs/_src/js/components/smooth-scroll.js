
// ----------------------------------------------------------------------------
//	Smooth Scroll
// ----------------------------------------------------------------------------
$(function(){
    var smoothScroll = function(options){
        var classname = options.classname,
        animationSpeed = options.animationSpeed,
        easing = options.easing,
        point = options.point;

        $(classname).click(function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                $(this).blur();
                var t = navigator.appName.match(/Opera/) ? "html" : "html,body";
                $(t).queue([]).stop();
                var $targetElement = $(this.hash);
                var scrollTo = $targetElement.offset().top;
                if (window.scrollMaxY) {
                    var maxScroll = window.scrollMaxY;
                } else {
                    var maxScroll = document.documentElement.scrollHeight - document.documentElement.clientHeight;
                }
                if (scrollTo > maxScroll){
                    scrollTo = maxScroll;
                }

                $t = $(t);
                $t.animate({ scrollTop: scrollTo - point }, animationSpeed, easing);

                var mousewheelevent = 'onwheel' in document ? 'wheel' : 'onmousewheel' in document ? 'mousewheel' : 'DOMMouseScroll';
                $(document).off( mousewheelevent );
                $(document).on( mousewheelevent, function(){
                    $(document).off( mousewheelevent );
                    $t.stop(true,false);
                });

                return false;
            }
        });
    }
    smoothScroll({ classname : '.js-smooth-scroll a',animationSpeed :800, easing : 'easeOutQuint', point:40 });
});
