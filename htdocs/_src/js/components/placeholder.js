// ----------------------------------------------------------------------------
// Placeholder
// ----------------------------------------------------------------------------	
$(function(){
    $.fn.setPlaceholder = function(options){
        $(this).each(function() {
            var $this = $(this);
            var def = $this.attr('title');
            if ($this.val() === '') {
                $this.val(def).addClass(options);
            } else if ($this.val() === def){
                $this.addClass(options);
            }
            $this.bind('focus', function(){
                if ($this.val() === def){
                    $this.val('').removeClass(options);
                }
            }).bind('blur', function(){
                if ($this.val() === def){
                    $this.addClass(options);
                } else if ($this.val() === ''){
                    $this.val(def).addClass(options);
                }
            });
            $this.attr('data-placeholder', 'true');
        });
		
        $('form:has([data-placeholder="true"])').bind('submit', function(){
            $('[data-placeholder="true"]', this).each(function(){
                if ($(this).val() === $(this).attr('title')){
                    $(this).val('');
                }
            });
            setTimeout(refill, 0);
        });
        var refill = function(){
            $('[data-placeholder="true"]').each(function(){
                if($(this).val() === ''){
                    $(this).val($(this).attr('title'));
                }
            });
        }
    };
	
	$('.placeholder').setPlaceholder('on');
});