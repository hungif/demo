// ----------------------------------------------------------------------------
// Accordion
// ----------------------------------------------------------------------------
$(function(){
  'use strict';

  (function(){
    var $btn;
    var JsAccordion = function($el) {
      this.$el = $el;
      this.init();
    };
    JsAccordion.prototype ={
      init: function() {
        var _this = this;
        this.$btn = this.$el.find('.accordion-btn');

        this.$btn.on('click', function() {
          $(this).toggleClass('on').next('.accordion-block').stop().slideToggle();

          if($(this).hasClass('on') ){
            $(this).nextAll('.accordion-btn').slice(0,1).css({'border-top': '1px solid #d0d2db'});
          }else{
            $(this).nextAll('.accordion-btn').slice(0,1).removeAttr('style');
          }
          if ($(this).find('.icon').size() > 0) _this.hasIcon($(this));
          if ($(this).find('.note').size() > 0) _this.hasNote($(this));
        });

          // 初期表示 
          if(this.$btn.hasClass('on')){
            this.$el.find('.on').next('.accordion-block').show();
            this.$el.find('.on').nextAll('.accordion-btn').slice(0,1).css({'border-top': '1px solid #d0d2db'});
            if (this.$btn.find('.icon').size() > 0) this.$el.find('.on .icon').text('ー');
            if (this.$btn.find('.note').size() > 0) this.$el.find('.on .note').text('閉じる');
          }


      },
      hasIcon: function(btn) {
        console.log(btn);
        if(btn.hasClass('on')){
          btn.find('.icon').text('ー');
        }else{
          btn.find('.icon').text('＋');
        }
      },
      hasNote: function(btn) {
        if(btn.hasClass('on')){
          btn.find('.note').text('閉じる');
        }else{
          btn.find('.note').text('見る');
        }
      }
    };

    if($('.js-accordion-01').size() > 0) var jsAccordion01 = new JsAccordion($('.js-accordion-01'));
    if($('.js-accordion-02').size() > 0) var jsAccordion02 = new JsAccordion($('.js-accordion-02'));
    if($('.js-accordion-03').size() > 0) var jsAccordion03 = new JsAccordion($('.js-accordion-03'));
    if($('.js-accordion-04').size() > 0) var jsAccordion04 = new JsAccordion($('.js-accordion-04'));

  }());
});
