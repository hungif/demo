// ----------------------------------------------------------------------------
// TOP-page- Header
// ----------------------------------------------------------------------------
$(function(){
    var topContent = function(){

        var winH,contH,int,pt,pb,contH2;

        function load(){
            $(window).on('load', function() {
                winH  = $(window).height();
                contH = $('#container').outerHeight();
                int   = winH - contH;
                onload(int);
                resize();
            });
        }

        function resize() {
            $(window).on('resize', function() {
                winH   = $(window).height();
                contH2 = contH;
                int    = winH - contH;
                onload(int);
            });
        }

        function onload() {
            pt    = int/2 + 30;
            pb    = int/2 - 30;
            $('.nav-top-01 ul .inner').css({'padding-top':pt,'padding-bottom':pb});
        }

        return{
            start: load
        };
    };
    if ( ua.uaObj.uaDevice !== 'device-mobile' && $('#header-top').size() > 0 ) {
        var module = topContent();
        module.start();
    }
});