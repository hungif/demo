// ----------------------------------------------------------------------------
// Contents Sort
// ----------------------------------------------------------------------------
$(function(){
    var ContentsSort = function(){
        var $container = $('.js-sort-container');
        var flag = true;

        $container.each(function(){
            var $btn = $(this).find('.js-sort-btn').find('li');
            var $list = $(this).find('.js-sort-content');

            $btn.on('click', function(){
                if(! $(this).hasClass('current') && flag === true ){
                    flag = false;
                    $btn.removeClass('current');
                    var tag = $(this).attr('class');
                    $(this).addClass('current');
                    if( tag == 'tag-all' ){
                        $list.find('.column').fadeOut(200, 'linear');
                        setTimeout(function(){
                            $list.find('.column').fadeIn(200, 'linear');
                        }, 200);
                    } else{
                        $list.find('.column').fadeOut(200, 'linear');
                        setTimeout(function(){
                            $list.find('.' + tag).fadeIn(400, 'linear');
                        }, 200);
                    }
                    flag = true;
                }
            });
        });
    }
    ContentsSort();
});
