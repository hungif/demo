
// ----------------------------------------------------------------------------
// Roll Over
// ----------------------------------------------------------------------------
$(function(){
    var RollOver = function(options){
        var roll = options.roll,
        unroll = options.unroll;

        var find_child = function(elm){
            var collect_img = {},
            collect_a   = {},
            counter_img = 0,
            counter_a   = 0;

            function get_elms(elm){
                elm.children().each(function(){
                    if($(this).attr('class') !== unroll){
                        if(this.tagName === 'IMG' || this.tagName === 'INPUT'){
                            collect_img[counter_img] = $(this);
                            counter_img++;
                        }else if(this.tagName === 'A'){
                            var tag_a = $(this);
                            tag_a.find('img').each(function(){
                                if($(this).attr('class') !== unroll){
                                    collect_img[counter_img] = $(this);
                                    counter_img++;
                                }
                            });
                        }

                        get_elms($(this));
                    }
                });
            }

            get_elms(elm);

            return [collect_img,collect_a];
        };

        $('.' + roll).each(function(){
            var items = find_child($(this)),
            items_img = items[0],
            onmouse_check = false;

            $.each(items_img, function(i){
                var btn = items_img[i];

                $(btn).on({
                    'mouseenter':function(){
                        if (!onmouse_check) {
                            onmouse_check = true;
                            btn.attr('src', btn.attr('src').replace(/(\.[^\.]+$)/, "_o$1"));
                        }
                    },
                    'mouseleave':function(){
                        btn.attr('src', btn.attr('src').replace(/_o/, ""));
                        onmouse_check = false;
                    }
                });
            });
        });
    };
    RollOver({ roll : 'js-rollover', unroll : 'js-unrollover' });
});
