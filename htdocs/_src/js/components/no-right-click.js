// ----------------------------------------------------------------------------
// No Right Click
// ----------------------------------------------------------------------------
$(function(){
    var NoRightClick = function(){
        var $noRightClick = $('.no-right-click');

        $noRightClick.on('contextmenu',function(e){
            return false;
        });
    }
    NoRightClick();
});
