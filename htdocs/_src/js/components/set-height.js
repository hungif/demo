// ----------------------------------------------------------------------------
// Match Height
// ----------------------------------------------------------------------------
$(function(){
    var setHeight = function(){
        $('.bullet-col2 li').matchHeight();
        $('.bullet-col3 li').matchHeight();
        $('.bullet-col4 li').matchHeight();
        $('.link-col2 li').matchHeight();
        $('.link-col3 li').matchHeight();
        $('.link-col4 li').matchHeight();
        $('.list-tag-01 li').matchHeight();
        $('.lyt-members-01 .column').matchHeight();
        $('.lyt-column-01 .box-contact-02').matchHeight();
        $('.box-content-01 a').matchHeight();
        $('.box-content-02 .title a').matchHeight();
        $('.box-content-02 .description').matchHeight();
        $('.box-content-02 .box-link').matchHeight();
        $('.box-content-03 .text p').matchHeight();
        $('.nav-category-01 .text').matchHeight();
        $('.nav-tab-01 .tab-btns span').matchHeight();
        $('.box-content-02 .title a').matchHeight();
        $('.nav-index-01 .column').matchHeight();
        $('.nav-index-02 .column').matchHeight();
        $('.nav-index-03 .text').matchHeight();
        $('.nav-tab-01 .tab-btns a').matchHeight();
    }

    $('body').bind('textresize', function () {
        setTimeout(function(){
            setHeight();
        }, 300);
    }).trigger('textresize');
});
