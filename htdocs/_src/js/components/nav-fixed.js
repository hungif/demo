
$(function(){
    var Navfixed = function(){
        var $win = $(window);
        var flag = true;
        var scrollPos = 0;

        var init = function(){
			eventSet();
		};

        var getScrollPosition = function (){
			try{
				return (document.documentElement.scrollTop || document.body.scrollTop);
			}catch(e){
				return 0;
			}
		};

        var getOffsetTopPosition = function($elm){
            return $elm.offset().top;
        };

        var onScroll = function(){
			var scrollPos = getScrollPosition();

            if( scrollPos > 200 ){
                $('#nav-fixed-right').addClass('slide-sp');
            } else{
                $('#nav-fixed-right').removeClass('slide-sp');
            }
		};

        var eventSet = function(){
            $win.on('scroll', function(event){
                onScroll();
            });
        };

        setTimeout(function(){
            $('#nav-fixed-right').addClass('slide');
        }, 2000);

        setTimeout(function(){
            if( flag === true){
                $('#nav-fixed-right').removeClass('slide');
                $('#nav-fixed-right').addClass('slide-back');
            }
        }, 4000);

        $('#nav-fixed-right').on({
            'mouseenter':function(){
                flag = false;
                $('#nav-fixed-right').removeClass('slide-back');
                $('#nav-fixed-right').addClass('slide');
            },
            'mouseleave':function(){
                $('#nav-fixed-right').addClass('slide-back');
                $('#nav-fixed-right').removeClass('slide');
            }
        });

        init();
    }
    Navfixed();
});
