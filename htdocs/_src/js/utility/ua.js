
// ----------------------------------------------------------------------------
// UA Check
// ----------------------------------------------------------------------------
var UaObj;

UaObj = (function() {
    function UaObj() {
        this.checkUA();
    }

    UaObj.prototype.uaObj = {
        uaDevice: 'device-pc',
        uaBrouser: false
    };

    UaObj.prototype.checkUA = function() {
        userAgent = navigator.userAgent.toLowerCase();
        appVersion = navigator.appVersion.toLowerCase();

        if (userAgent.indexOf('iphone') > 0 || userAgent.indexOf('ipod') > 0 || userAgent.indexOf('android') > 0) {
            this.uaObj.uaDevice = 'device-mobile';
        }

        if (userAgent.indexOf('ipad') > 0 ) {
            this.uaObj.uaDevice = 'ipad';
        }

        if (userAgent.indexOf('opera') !== -1) {
            return this.uaObj.uaBrouser = 'opera';
        } else if (userAgent.indexOf("msie") !== -1) {
            if (appVersion.indexOf("msie 6.") !== -1) {
                return this.uaObj.uaBrouser = 'ie6';
            } else if (appVersion.indexOf("msie 7.") !== -1) {
                return this.uaObj.uaBrouser = 'ie7';
            } else if (appVersion.indexOf("msie 8.") !== -1) {
                return this.uaObj.uaBrouser = 'ie8';
            } else if (appVersion.indexOf("msie 9.") !== -1) {
                return this.uaObj.uaBrouser = 'ie9';
            } else {
                return this.uaObj.uaBrouser = 'ie10';
            }
        } else if (userAgent.indexOf('trident') !== -1) {
            return this.uaObj.uaBrouser = 'ie11';
        } else if (userAgent.indexOf('chrome') !== -1) {
            return this.uaObj.uaBrouser = 'chrome';
        } else if (userAgent.indexOf('safari') !== -1) {
            return this.uaObj.uaBrouser = 'safari';
        } else if (userAgent.indexOf('firefox') !== -1) {
            return this.uaObj.uaBrouser = 'firefox';
        } else {
            return this.uaObj.uaBrouser = false;
        }
    };

    return UaObj;

})();

ua = new UaObj();
