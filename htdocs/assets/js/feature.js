(function($) {
    'use strict';
    var animationFlipFrame;
    $(window).on('load', function() {

        var App = function() {
                this.init();
            },
            app;

        App.prototype.init = function() {
            var self = this;
            self.event = {
              touchstart: self.touch? 'touchstart': 'mousedown',
              touchmove: self.touch? 'touchmove': 'mousemove',
              touchend: self.touch? 'touchend': 'mouseup',
              orientationchange: self.orientation && !self.Android? 'orientationchange': 'resize',
              wheel: 'onwheel' in document ? 'wheel' : 'onmousewheel' in document ? 'mousewheel' : 'DOMMouseScroll'
            };
            self.obj = {
                sec01: $('.lyt-feature-inner-01'),
                sec02: $('.lyt-feature-inner-02'),
                sec03: $('.lyt-feature-inner-03'),
                hdg01: $('.hdg-level1-feature-01'),
                txt01: $('.txt-level1-feature-01')
            }
            self.obj.hdg01.fadeIn(2000);
            self.obj.txt01.delay(200).fadeIn(2000);
            self.setScreen();
            self.setWindowChange();
            window.addEventListener(self.event.orientationchange, self.setWindowChange.bind(self), false);
            window.addEventListener(self.event.wheel, self.scroll.bind(self), false);
            self.sectionScreen = [
              { el: document.getElementById('feature-section01'), id: 'feature-section01', top: 0, center: 0, bottom: 0, fromTop: false, fromBottom: false, animating: false, frame: {17: 200}, end: null },
              { el: document.getElementById('feature-section02'), id: 'feature-section02', top: 0, center: 0, bottom: 0, fromTop: false, fromBottom: false, animating: false, frame: {}, end: 24 },
              { el: document.getElementById('feature-section03'), id: 'feature-section03', top: 0, center: 0, bottom: 0, fromTop: false, fromBottom: false, animating: false, frame: {}, end: 24 },
            ];
        }
        var FlipAnimation = function(animationID, speed, delay, animationController, end) {
            var self = this;
                self.animationID = animationID,
                self.animationSpeed = speed,
                self.delay = delay,
                self.animationController = animationController,
                self.end = end,
                self.animationPadding,
                self.currentX,
                self.currentY,
                self.currentNum,
                self.currentClmNum;
                self.animationSpriteSheet = $('#' + animationID),
                self.animationSpriteSheetWidth = self.animationSpriteSheet.width(),
                self.animationSpriteSheetHeight = self.animationSpriteSheet.height(),
                self.spriteSheetSource = self.animationSpriteSheet.find('.spriteSheetSource img'),
                self.spriteSheetSourceWidth = self.spriteSheetSource.width() /2,
                self.spriteSheetSourceHeight = self.spriteSheetSource.height() /2,
                self.animationSpriteSheetColumn = Math.floor(self.spriteSheetSourceWidth / self.animationSpriteSheetWidth),
                self.animationSpriteSheetRow = Math.floor(self.spriteSheetSourceHeight / self.animationSpriteSheetHeight),
                self.endedNum = (self.end) ? self.end : parseInt(self.animationSpriteSheetRow * self.animationSpriteSheetColumn);

            // Initialize
            //===========================->
            var initialize = function() {
                self.animationPadding = 0;
                self.currentX = 0;
                self.currentY = 0;
                self.currentNum = 1;
                self.currentClmNum = 0;
                self.animationSpriteSheet.css({
                    'background-position': '0px 0px',
                    'background-size': '1026px 700px'
                });
            }
            initialize();
            clearTimeout(self.animationFlipFrame);
            var animeFunc = function() {
                self.currentNum++;
                self.currentClmNum++;
                self.currentX += parseInt(self.animationSpriteSheetWidth) + parseInt(self.animationPadding);
                if (self.animationSpriteSheetColumn === self.currentClmNum) {
                    self.currentX = 0;
                    self.currentY += parseInt(self.animationSpriteSheetHeight) + parseInt(self.animationPadding);
                    self.currentClmNum = 0;
                };
                self.animationSpriteSheet.css({
                    'background-position': -1 * self.currentX + 'px ' + -1 * self.currentY + 'px',
                    'background-size': '1026px 700px'
                })
                if (self.currentNum !== self.endedNum) {
                    if (self.currentNum in self.animationController) {
                        self.animationFlipFrame01 = setTimeout(animeFunc, self.animationController[self.currentNum]);
                    } else {
                        self.animationFlipFrame = setTimeout(animeFunc, self.animationSpeed);
                    }
                } else {
                    setTimeout(function() {
                        initialize();
                        self.animationFlipFrame = setTimeout(animeFunc, self.delay);
                    }, self.delay * 2.5);
                }
            };
            self.animationFlipFrame = setTimeout(animeFunc, self.delay);
        },
        flipAnimation01, flipAnimation02;

        App.prototype.setScreen = function() {
            var self = this,
                originalPadding = 10,
                animationSpriteSheet = document.getElementsByClassName('animationSpriteSheet');
            Array.prototype.forEach.call(animationSpriteSheet, function(el) {
                var img = el.getElementsByTagName('img')[0],
                    p, w, h,
                    imgLoaded = function() {
                        w = img.clientWidth;
                        h = img.clientHeight;
                        el.style.backgroundImage = 'url(' + img.getAttribute('src') + ')';
                        //el.style.backgroundSize = (w / 200 * 100) + '% auto';
                        //self.orientationchange();
                    };
                if (img.clientWidth) {
                    imgLoaded();
                } else {
                    img.onload = imgLoaded;
                }
            });
        };

        App.prototype.scroll = function(ev) {
        //   var self = this, st = document.documentElement.scrollTop || document.body.scrollTop,
        //       windowCenter = st + self.windowHeight / 2,
        //       windowBottom = st + self.windowHeight,
        //       i;
        //   for(i = 0; i < self.sectionScreen.length; i++) {
        //     if(st > self.sectionScreen[i].el.offsetTop + self.windowHeight) {
        //       // スクリーンより上
        //       self.sectionScreen[i].fromTop = true;
        //       self.sectionScreen[i].fromBottom = false;
        //       if(self.sectionScreen[i].animating === true) {
        //         self.sectionScreen[i].animating = false;
        //       }
        //   } else if(windowBottom < (self.windowHeight < self.sectionScreen[i].el.offsetTop ? self.sectionScreen[i].el.offsetTop : self.windowHeight + 1)) {
        //       // スクリーンより下
        //       self.sectionScreen[i].fromTop = false;
        //       self.sectionScreen[i].fromBottom = true;
        //       if(self.sectionScreen[i].animating === true) {
        //         self.sectionScreen[i].animating = false;
        //       }
        //     }
        //     if(
        //       // 下からスクロールしてスクリーン3/4超えたら
        //       (self.sectionScreen[i].fromBottom === true && (windowCenter + self.windowHeight / 4) > self.sectionScreen[i].center && self.sectionScreen[i].animating !== true)
        //       ||
        //       // 上からスクロールしてスクリーン1/4超えたら
        //       (self.sectionScreen[i].fromTop === true && (windowCenter - self.windowHeight / 4) < self.sectionScreen[i].center && self.sectionScreen[i].animating !== true)
        //     ) {
        //       self.sectionScreen[i].animating = true;
        //         if(self.sectionScreen[i].id === 'feature-section02') {
        //             self.sectionScreen[1].el.childNodes[1].play()
        //             console.dir(self.sectionScreen[i].el);
        //         }
        //     }
        //   }
        };

        App.prototype.setWindowChange = function(ev) {
          var self = this;
          self.windowWidth = document.documentElement.clientWidth;
          self.windowHeight = document.documentElement.clientHeight;
          var header = document.querySelector('#header');
          var navLocation = document.querySelector('#nav-location');
          var $headerHeight = header.clientHeight;
          var $navLocationHeight = navLocation.clientHeight;
          var featureSec01 = document.querySelector('.lyt-feature-inner-01');
          var featureSec02 = document.querySelector('.lyt-feature-inner-02');
          var featureSec03 = document.querySelector('.lyt-feature-inner-03');
          featureSec01.style.height = (self.windowHeight - $headerHeight - $navLocationHeight) + 'px';
          featureSec02.style.height = self.windowHeight + 'px';
          featureSec03.style.height = self.windowHeight + 'px';
        };

    var app = new App();
    //var flipAnimation01 = new FlipAnimation('animation01', 500, 500, {});

    // var controller = new ScrollMagic.Controller();
    // var animateSec02 = new ScrollMagic.Scene({
    //         triggerElement: ".lyt-feature-inner-02",
    //         triggerHook: "onLeave",
    //         duration: app.windowHeight /2
    //     })
    //     .setPin('.lyt-feature-inner-02')
    //     .addIndicators()
    //     controller.addScene([
    //         animateSec02
    //     ]);
     });

})(jQuery);
