function selectStyleSheet( ) {
	var WIN_IE4_SRC = "/english/common/css/win_ie.css";
	var WIN_IE5_SRC = "/english/common/css/win_ie.css";
	var WIN_NN4_SRC = "/english/common/css/win_nn4.css";
	var WIN_NN6_SRC = "/english/common/css/win_nn.css";
	var WIN_OPERA_SRC = "/english/common/css/win_ie.css";

	var MAC_IE4_SRC = "/english/common/css/mac_ie.css";
	var MAC_IE5_SRC = "/english/common/css/mac_ie.css";
	var MAC_NN4_SRC = "/english/common/css/mac_nn4.css";
	var MAC_NN6_SRC = "/english/common/css/mac_nn.css";
	var MAC_OPERA_SRC = "/english/common/css/mac_nn.css";

	// put the browser information in variable
	var os      = getOsName( );
	var browser = getBrowserName( );
	var version = getBrowserVersion( ).charAt( 0 );

	var cssSrc = null;

	if ( os == "Windows" ) {
		if ( browser == "Explorer" ) {
			if ( version == 4 ) {
				cssSrc = WIN_IE4_SRC;
			}
			if ( version >= 5 ) {
				cssSrc = WIN_IE5_SRC;
			}
		} else if ( browser == "Netscape" ) {
			if ( version == 4 ) {
				cssSrc = WIN_NN4_SRC;
			}
			if ( version >= 5 ) {
				cssSrc = WIN_NN6_SRC;
			}
		} else if ( browser == "OPERA" ) {
			cssSrc = WIN_OPERA_SRC;
		}
	} else if ( os == "MacOS" ) {
		if ( browser == "Explorer" ) {
			if ( version == 4 ) {
				cssSrc = MAC_IE4_SRC;
			}
			if ( version >= 5 ) {
				cssSrc = MAC_IE5_SRC;
			}
		} else if ( browser == "Netscape" ) {
			if ( version == 4 ) {
				cssSrc = MAC_NN4_SRC;
			}
			if ( version >= 5 ) {
				cssSrc = MAC_NN6_SRC;
			}
		} else if ( browser == "OPERA" ) {
			cssSrc = MAC_OPERA_SRC;
		}
	}

	// output the stylesheet
	if ( cssSrc ) {
		document.open();
		document.write('<link href="' + cssSrc + '" rel="stylesheet" type="text/css">');
		document.close();
	}
}

selectStyleSheet();
