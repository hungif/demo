(function ($) {
    $(document).ready(function () {

        var hash = location.hash.split("?")[0];

        //console.log(hash);
        if(hash != ""){
            var target = $(hash);
            var position = target.offset().top;
            $('body,html').animate({scrollTop:position}, 400, 'swing');
        }



        var titleSpeed = 500;

        function animateCampaign(){
            $("#n_campaign_title").animate({"top": "0%"}, titleSpeed);
            $("#n_campaign_content").delay(200).animate({"top": "0%"},500);
        }

        function animateNews(){
            $("#n_news_title").animate({"top": "0%"}, titleSpeed);
            $("#sec_news_cat").delay(200).animate({"top": "107px"},500);
           // $("#sec_news_slogan").delay(200).animate({"top": "40px"},500);
            $("#sec_news_slogan").animate({
                "top": "40px"
            },
            {
                duration: 1500, // how fast we are animating
                easing: 'easeInOutBack' // the type of easing
            });
            $("#sec_news_left").delay(600).animate({"left": "60px"},500);
            $("#sec_news_place").delay(600).animate({"left": "640px"},500);
            $("#sec_news_map").delay(800).animate({"left": "640px"},500);
        }

        function animateMovie(){
            $("#n_movie_title").animate({"top": "0%"}, titleSpeed);
            $("#n_movie_text").animate({"left": "112px"},500);
            $("#n_movie_img").animate({"left": "112px"},500);
        }

        function animateGallery(){
            $("#n_gallery_title").animate({"top": "0%"}, titleSpeed);
            $("#n_gallery_content").delay(200).animate({"top": "0%"},500);
            $("#sec_gallery_bubble").animate({"top": "25px"},500);
        }

        function animateService(){
            $("#n_service_title").animate({"top": "0%"}, titleSpeed);
            $("#n_service_left").delay(200).animate({"left": "59px"},500);
            $("#n_btn2").delay(200).animate({"left": "59px"},500);
            $("#n_service_right").delay(200).animate({"right": "40px"},500);
            $("#sec_service_bubble").delay(200).animate(
                {"top": "15px"},
                {
                    duration: 1000, // how fast we are animating
                    easing: 'easeInOutBack' // the type of easing
                }
            );
        }



        $('#news').scrollTrigger({
            callback: function () {
                animateNews();
            }
        });

        $('#campaign').scrollTrigger({
            callback: function () {
                animateCampaign();
            }
        });
        //$('#news').scrollTrigger({
        //    callback: function () {
        //        animateNews();
        //    }
        //});

        $('#movie').scrollTrigger({
            callback: function () {
                animateMovie();
            }
        });

        $('#gallery').scrollTrigger({
            callback: function () {
                animateGallery();
            }
        });

        $('#service').scrollTrigger({
            callback: function () {
                animateService();
            }
        });


        $(window).trigger('scroll');


    });
})(window.jQuery);