navigator.sayswho = (function() {
    var N = navigator.appName,
        ua = navigator.userAgent,
        tem;
    var M = ua.match(/(opera|chrome|safari|firefox|msie)\/?\s*(\.?\d+(\.\d+)*)/i);
    if (M && (tem = ua.match(/version\/([\.\d]+)/i)) != null) M[2] = tem[1];
    M = M ? [M[1], M[2]] : [N, navigator.appVersion, '-?'];
    return M;
})();

var platform = 'unknown',
    browser = navigator.sayswho[0],
    browser = browser.toLowerCase();
browserVersion = navigator.sayswho[1];

if (navigator.appVersion.indexOf("Android") != -1) platform = "android";
if (navigator.appVersion.indexOf("Win") != -1) platform = "windows";
if (navigator.appVersion.indexOf("Mac") != -1) platform = "mac";
if (navigator.appVersion.indexOf("X11") != -1) platform = "unix";
if (navigator.appVersion.indexOf("Linux") != -1) platform = "linux";
if ((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i)) || (navigator.userAgent.match(/iPad/i))) {
    platform = 'ios';
}

jQuery(document).ready(function($) {

    var $htmlbody = $('html,body');
    var $body = $('body');
    var $win = $(window);
    var $doc = $(document);
    var $parallaxItem = $('.parallax');
    var devices = platform === 'ios' || platform === 'linux' || platform === 'android';
    var slideOffsetTopArray = [];
    var scrollSpeed = 1000;
    var winH, winW;
    var sT = $body.scrollTop();
    var list = [];
    var lastPos = 0;
    var animate;

    if (devices) $body.addClass('device');

    var queue = [];
    var fn = window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.oRequestAnimationFrame ||
        window.msRequestAnimationFrame ||
            function( /* function */ callback, /* DOMElement */ element) {
                window.setTimeout(callback, 1000 / 10);
        };

    animate = {
        stopped: true,
        add: function(callback, name) {
            var theAnim = {
                name: name,
                callback: callback,
                stopped: false
            }
            queue.push(theAnim);
        },
        remove: function(name) {
            for (var i = 0; i < queue.length; i++) {
                if (queue[i].name === name) {
                    queue.splice(i, 1);
                }
            }
        },
        anim: function() {
            var scope = this;
            if (!this.stopped) {
                var callback;
                for (var i = 0; i < queue.length; i++) {
                    if (!queue[i].stopped) {
                        callback = queue[i].callback;
                        callback();
                    }
                }
                fn(function() {
                    scope.anim();
                });
            }
        },
        start: function(name) {
            if (typeof name === 'undefined') {
                if (this.stopped !== false) {
                    this.stopped = false;
                    this.anim();
                }

            } else {
                for (var i = 0; i < queue.length; i++) {
                    if (queue[i].name === name) {
                        queue[i].stopped = false;
                    }
                }
            }
        },
        stop: function(name) {
            if (typeof name === 'undefined') {
                this.stopped = true;
            } else {
                for (var i = 0; i < queue.length; i++) {
                    if (queue[i].name === name) {
                        queue[i].stopped = true;
                    }
                }
            }
        },
        clearQueue: function(stop) {
            queue = [];
            if (stop) {
                this.stopped = true;
            }
        }
    }



    $win.on('scroll', function() {

        topD = $doc.scrollTop();
        winH = $win.height();


        sT = $doc.scrollTop();

        if (animate.stopped) {
            animate.start();
        }

    });




    var resize = function() {

        var winH = $win.height();

        for (var i = 0; i < list.length; i++) {
            list[i].thisTop = list[i].e.parent().offset().top;
        }

    }

    resize();
    $win.on('resize', resize);

    var parallax = function() {

        if (lastPos === sT) {
            animate.stop();
        }

        if (Math.abs(sT - lastPos) < .2);
        if (lastPos < 0) lastPos = 0;
        lastPos += (sT - lastPos) * .1;

        for (var i = 0; i < list.length; i++) {
            var item = list[i];
            var mins = 0;
            var el = item.e;
            var thisTop = item.thisTop;
            var speed = item.s;
            var dif = winH * speed;
            var y = (thisTop - (winH + lastPos)) * speed + dif;

            if (Modernizr.csstransforms3d) {
                el.css({
                    'transform': 'translate3d(0,' + y + 'px,0)',
                    '-webkit-transform': 'translate3d(0,' + y + 'px,0)',
                    '-moz-transform': 'translate3d(0,' + y + 'px,0)',
                    '-o-transform': 'translate3d(0,' + y + 'px,0)',
                    '-ms-transform': 'translate3d(0,' + y + 'px,0)'
                })
            } else {
                // Do Nothing..
            }
        }
    }

    var init = function() {
        $('.parallax-item').each(function() {
            var el = $(this);
            var speed = el.data('speed');
            var timing = [0, 99999999];
            var obj = {
                timing: timing,
                e: el,
                s: speed,
                t: 0,
                thisTop: el.parent().offset().top
            };

            list.push(obj);
        })

        animate.add(function() {
            parallax()
        }, 'parallax-item');
        animate.start();
    }

    if (!devices) init();


});