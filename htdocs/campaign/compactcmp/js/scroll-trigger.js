/**
 * Created by wushuyi on 2015/8/14.
 */
(function (win, $) {
    var pubsub = function pubsub() {
        var subscriptions = [];

        var unsubscribe = function unsubscribe(onChange) {
            var id = $.inArray(onChange, subscriptions);
            if (id >= 0) subscriptions.splice(id, 1);
        };

        var subscribe = function subscribe(onChange) {
            subscriptions.push(onChange);
            var dispose = function dispose() {
                return unsubscribe(onChange);
            };
            return {dispose: dispose};
        };

        var push = function push(value) {
            $.each(subscriptions, function (index, subscription) {
                  subscription && subscription(value);
            });
        };

        return {subscribe: subscribe, push: push, unsubscribe: unsubscribe};
    };

    var task = pubsub();

    var $win = $(win);

    var winScrollY, winHeight;
    winHeight = $win.height();
    $win.on('resize', function(evt){
        winHeight = $win.height();
    });
    $win.on('scroll', function (evt) {
        winScrollY = (win.pageYOffset !== undefined) ? win.pageYOffset :
            (document.documentElement || document.body.parentNode || document.body).scrollTop;
        task.push(winScrollY + winHeight);
    });

    $.fn.scrollTrigger = function (options) {
        var self = this;
        var $self = $(this);
        var defaultOpt = {
            top: 0,
            callback: function(){}
        };
        var opts = $.extend({}, defaultOpt, options);
        //console.log(opts);
        //var triggerHeight = $self.offset().top + (opts.top || $self.height());
        var triggerHeight = $self.offset().top + 300;
        var proxy = function proxy(scroll) {
            if (opts.callback && scroll > triggerHeight) {
                opts.callback.apply(self, [scroll]);
                task.unsubscribe(proxy);
            }
        };
        task.subscribe(proxy);
    };
})(window, jQuery);