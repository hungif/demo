$(document).ready(function () {
	
	/*-----------------------
		SP Redirect
	-----------------------*/
	var ua = navigator.userAgent;
	
	function isMobile(){
			if( ua.indexOf("iPhone") >= 0 || ( ua.indexOf("Android") >= 0 && ua.indexOf("Mobile") >= 0 ) ){
					return true;
			}else{
					return false;
			}
	}
	
	if( isMobile() ){
			location.replace( '/smp/campaign/compactcmp/' );
	}
	
	
	
	var duration = 1000,
	easing = 'easeInOutCubic',
	delayTime = 10;
	
	$mainVElm = $('#mainV .motionElm');
	$mainVElm.css({
				
				"opacity":0,
				"display": "block"
	});
	
	function showMainVElm() {
		$mainVElm.delay(delayTime).each(function( index ) {
		var targetPosition = $(this).position().top;
		var startPosition = $(this).parent().height();
		var delayTime = index * 200;
		
			$(this).delay(delayTime).css("top",startPosition).animate(
				{
					"top":targetPosition,
					"opacity":"1.0"
					},duration,easing
			);
		});
	}
	
	$serviceElm = $('#services .motionElm');
	$serviceElm.css({
				
				"opacity":0,
				"display": "block"
	});
	
	function showServiceElm() {
		if(!$serviceElm.hasClass('animated')){
			$serviceElm.addClass('animated');
			$serviceElm.delay(delayTime).each(function( index ) {
			var targetPosition = $(this).position().top;
			var startPosition = $(this).parent().height();
			var delayTime = index * 200;
			
				$(this).delay(delayTime).css("top",startPosition).animate(
					{
						"top":targetPosition,
						"opacity":"1.0"
						},duration,easing
				);
			});
		}
	}
	
	//prevent context menu
	$(function(){
		$('img').on('contextmenu',function(e){
			return false;
		});
	});


 var $modalWrap	= $('.modalWrap');
 $modalWrap.css('display','none');

// menu clone
	$('.gNavWrap').addClass('original').clone().insertAfter('.gNavWrap').addClass('sticky').css('position','fixed').css('top','0').css('margin-top','0').css('z-index','500').removeClass('original').hide();
	
	
	//scrollIntervalID = setInterval(stickIt, 100);

	/*stickerNav*/
	//var sclNum = 0;
	//$(window).scroll(function(){
	//	stickIt();
	//});
	//
	//function stickIt() {
	//
	//	var orgElementPos = $('.original').offset();
	//	orgElementTop = orgElementPos.top;
	//
	//	if ($(window).scrollTop() >= (orgElementTop)) {
	//		// only show the cloned, sticky element.
	//
	//		orgElement = $('.original');
	//		coordsOrgElement = orgElement.offset();
	//		leftOrgElement = coordsOrgElement.left;
	//		widthOrgElement = orgElement.css('width');
	//		$('.sticky').css('left',leftOrgElement+'px').css('top',0).css('width',widthOrgElement).show();
	//		$('.original').css('visibility','hidden');
	//		$('.sticky .navPagetop').fadeIn(700);
	//	} else {
	//		// not scrolled past the menu; only show the original menu.
	//
	//		$('.sticky').hide();
	//		$('.original').css('visibility','visible');
	//	}
	//}
	//	var $motionElm = $('#services .motionElm');
		//$motionElm.css('visibility','hidden');
		
//	$(window).scroll(function(){
//	 var windowHeight = $(window).height(),
//			 topWindow = $(window).scrollTop();
//			 
//	 $motionElm.each(function(){
//		var targetPosition = $(this).offset().top;
//		if(topWindow > targetPosition - windowHeight + 100){
//		 showServiceElm();
//		}
//	 });
//	});	

		
	$(".inlineModal").colorbox({inline:true, width:"660px"});
	$(".movieModal").colorbox({inline:true, width:"660px"});
	
	/* smooth scroll */
	var $fixedHeaderHeight = $('.gNavWrap').height();
	$("a.pageLink[href^=#]").click(function(){
		var Hash = $(this.hash);
		var HashOffset = $(Hash).offset().top;
		$("html,body").animate({ scrollTop: HashOffset-$fixedHeaderHeight}, 'slow','swing');
		return false;
	});


	$(window).load(function() {
			$('.loadingIndicator').fadeOut();
			showMainVElm();
			showServiceElm();
			
	});//window load

});//window ready


