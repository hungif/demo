$(function(){
	$("h3, .close").hover(function(){
			$(this).css("cursor","pointer");
		},function(){
			$(this).css("cursor","default");
	});
	
	$(".accordion > .a_contents").after().hide();


	$("#anchor01 h3").click(function(){
		$(this).next().slideToggle();
		$(this).toggleClass("active");
	});
	$("#anchor01 .close").click(function(){
		$("#anchor01 h3").removeClass("active");
		$(this).parent().slideToggle();
	});

	
	$("#anchor02 h3").click(function(){
		$(this).next().slideToggle();
		$(this).toggleClass("active");
	});
	$("#anchor02 .close").click(function(){
		$("#anchor02 h3").removeClass("active");
		$(this).parent().slideToggle();
	});


	$("#anchor03 h3").click(function(){
		$(this).next().slideToggle();
		$(this).toggleClass("active");
	});
	$("#anchor03 .close").click(function(){
		$("#anchor03 h3").removeClass("active");
		$(this).parent().slideToggle();
	});


	$("#anchor04 h3").click(function(){
		$(this).next().slideToggle();
		$(this).toggleClass("active");
	});
	$("#anchor04 .close").click(function(){
		$("#anchor04 h3").removeClass("active");
		$(this).parent().slideToggle();
	});


});

