document.write('<div class="gnav-201105">');
document.write('<ul class="business"><!-- ulのclassで、何がハイライトするか決められます -->');
document.write('<li class="www"><a href="http://www.nikkeibp.co.jp/" title="nikkei BPnet :: 日経BPのビジネス情報ポータル">BPnet<span></span></a></li>');
document.write('<li class="business"><a href="http://business.nikkeibp.co.jp/" title="NBonline（日経ビジネスオンライン） :: ビジネスリーダーの羅針盤">ビジネス<span></span></a></li>');
document.write('<li class="pc"><a href="http://pc.nikkeibp.co.jp/" title="PConline（日経パソコン オンライン） :: 仕事に役立つパソコン総合情報">PC<span></span></a></li>');
document.write('<li class="itpro"><a href="http://itpro.nikkeibp.co.jp/" title="ITpro :: ITプロフェッショナルのための専門情報">IT<span></span></a></li>');
document.write('<li class="techon"><a href="http://techon.nikkeibp.co.jp/" title="Tech-On! :: 技術者を応援する総合情報サイト">テクノロジー<span></span></a></li>');
document.write('<li class="medical"><a href="http://medical.nikkeibp.co.jp/" title="日経メディカル オンライン :: 臨床医のための情報サイト">医療<span></span></a></li>');
document.write('<li class="kenplatz"><a href="http://kenplatz.nikkeibp.co.jp/" title="KEN-Platz（ケンプラッツ） :: 建設・不動産の総合情報サイト">建設・不動産<span></span></a></li>');
document.write('<li class="eco"><a href="http://eco.nikkeibp.co.jp/" title="ECO JAPAN（エコジャパン） :: 環境情報サイト">環境<span></span></a></li>');
document.write('<li class="trendy"><a href="http://trendy.nikkeibp.co.jp/" title="nikkei TRENDYnet :: トレンドウォッチ ポータル">TRENDYnet<span></span></a></li>');
document.write('<li class="wol"><a href="http://wol.nikkeibp.co.jp/" title="日経ウーマンオンライン :: はたらくわたし、きれいなわたし">働く女性<span></span></a></li>');
document.write('<li class="rebuild"><a href="http://rebuild.nikkeibp.co.jp/" title="復興ニッポン :: いま、歩き出す未来への道">復興ニッポン<span></span></a></li>');
document.write('<li class="career"><a href="http://career.nikkei.co.jp/" title="日経キャリアNET :: 質の高い転職・求人情報">転職<span></span></a></li>');
document.write('<li class="nng"><a href="http://nng.nikkeibp.co.jp/" title="NATIONAL GEOGRAPHIC :: 地球の素顔をビジュアルに伝える">自然と動物<span></span></a></li>')
document.write('<li class="nikkei"><a href="http://www.nikkei.com/" title="日本経済新聞 電子版">日経電子版<span></span></a></li>');
document.write('</ul>');
document.write('</div>');
