/* ----------------------------------------
 * JavaScript for HandsFreeTravel
 * ----------------------------------------
 */
var HandsFreeTravel = HandsFreeTravel || {};
var _module;

// for debug
HandsFreeTravel.debug = false; // debug mode
HandsFreeTravel.log = true; // permission for showing log

// constructor
$(function()
{
	// module
	_module = new HandsFreeTravel.Module();
});




/* ----------------------------------------
 * CLASS - Module
 * ----------------------------------------
 */
HandsFreeTravel.Module = function() {
	var _self = this;
	
	// ua
	this.ua;
	
	// dom
	this.container = $('#container');
	this.btn = $('#btn_pagetop');
	
	// type
	this.type = 0;
	// defY
	this.defY;
	
	
	/* ------------------
	 * init
	 */
	this.init = function() {
		// ua
		this.ua = new HandsFreeTravel.UserAgent();
		
		// dom
		this.btn.children('a').css({ opacity : 0 });
		
		// defY
		this.defY = parseInt(_self.btn.css('bottom'));
		
		// scroll
		$(window).bind({
			scroll : this._scroll
		});
		this._scroll();
		
		$('.hft_item').each(function() {
			var h = $(this).children('.hft_header').height();
			var y = Math.max(0, Math.round((h - 44) / 2));
			$(this).children('.hft_header').children('p').css({ marginTop : y });
		});
		
	}

	/* ------------------
	 * _scroll
	 */
	this._scroll = function() {
		var scrlY = $(window).scrollTop();
		var winH = _self.ua.isiPhone ?  window.innerHeight : $(window).height();
		var bodyH = document.body.clientHeight;
		//_log('scroll:' + scrlY + ' | bodyH:' + bodyH);
		
		var vy = $('#btn_back2map').length > 0 ? -90 : 0;
		var minY = 200;
		var maxY = _self.container.offset().top + _self.container.outerHeight() - winH + _self.defY + vy;
		
		if(scrlY < minY) {
			if(_self.type > 0) {
				_self.hide();
			}
			_self.type = 0;
		} else  {
			if(_self.type == 0) {
				_self.show();
			}
			if(maxY < scrlY) {
				var y = _self.defY + scrlY - maxY;
				_self.btn.css({ bottom : y });
				_self.type = 2;
			} else {
				_self.btn.css({ bottom : _self.defY });
				_self.type = 1;
			}
		}
	}

	/* ------------------
	 * show
	 */
	this.show = function() {
		this.btn.css({ display : 'block' }).children('a').stop().animate({ opacity : 1 }, 500, 'linear');
	}

	/* ------------------
	 * hide
	 */
	this.hide = function() {
		this.btn.children('a').stop().animate({ opacity : 0 }, 500, 'linear', function() {
			_self.btn.css({ display : 'none' });
		});
	}
	
	this.init();
	return this;
}





/* ----------------------------------------
 * CLASS - UserAgent
 * ----------------------------------------
 */
HandsFreeTravel.UserAgent = function() {
	var doc = document;
	var _ua = window.navigator.userAgent.toLowerCase(),
			_iPhone, _iPad, _iPod, _iOSver,
			_Android, _AndroidMobile, _AndroidTablet, _AndroidVer,
			_WindowsPhone,
			_bot;

	// devices
	if (_ua.indexOf("iphone") != -1) {
		_iPhone = true;
		_ua.match(/iphone os (\d+)_(\d+)/);
		_iOSver = RegExp.$1*1 + RegExp.$2*0.1;

	} else if (_ua.indexOf("ipad") != -1) {
		_iPad = true;
		_ua.match(/cpu os (\d+)_(\d+)/);
		_iOSver = RegExp.$1*1 + RegExp.$2*0.1;

	} else if (_ua.indexOf("ipod") != -1) {
		_iPod = true;
		_ua.match(/os (\d+)_(\d+)/);
		_iOSver = RegExp.$1*1 + RegExp.$2*0.1;

	} else if (_ua.indexOf("android") != -1) {
		_Android = true;
		_ua.match(/android (\d+\.\d)/);
		_AndroidVer = parseFloat(RegExp.$1);

		if(_ua.indexOf('mobile') != -1) {
			_AndroidMobile = true;
		} else {
			_AndroidTablet = true;
		}
	} else if (_ua.indexOf("windows phone") != -1) {
		_WindowsPhone = true;
	}

	// if it is crawler
	if(_ua.indexOf('googlebot') != -1 || _ua.indexOf('yahoo') != -1 || _ua.indexOf('msnbot') != -1) {
		_bot = true;
	}

	var ua = {
		isiPhone : _iPhone ,
		isiPad : _iPad,
		isiPod : _iPod,
		isiOS : (_iPhone || _iPad || _iPod),
		isAndroid : _Android,
		isAndroidMobile : _AndroidMobile,
		isAndroidTablet : _AndroidTablet,
		isWindowsPhone : _WindowsPhone,
		isBot : _bot
	}

	return ua;
}





/* ----------------------------------------
 * Utility
 * ----------------------------------------
 */

/* String.dig */
if(!String.prototype.dig) {
	String.prototype.dig = function() {
		var n = Number(this);
		if(n == 0) {
			return '000';
		} else if(n < 10) {
			return ('00' + this);
		} else if(n < 100) {
			return ('0' + this);
		} else {
			return this;
		}
	}
}

/**
 * UTILS : _LOG
 */
var _log = function(value) {
	if(HandsFreeTravel.log) {
		if (this.console && typeof console.log != "undefined"){
			console.log(value);
			//$('#ios_debug').prepend(value + '<br>\n');
		}
	}
}










