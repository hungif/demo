/*    Global Function
-------------------------------------------------*/
var globalFunc = {};
globalFunc.preload = function (imgs){
	for(var i = 0; i < imgs.length; i++){
		var imgObj = new Image();
		imgObj.src = imgs[i];
	}
};
var ymtFlg = false;

(function(){

$(function(){
	window.addEventListener('load', function(){
		main();
		setTimeout(function(){ if (window.pageYOffset === 0) { window.scrollTo(0,1); } }, 100);
	}, false);
});

/*    main
-------------------------------------------------*/
function main(){


	// side menu
	$('#menuBtn').slideMenu({
		main_contents: "#mainContent"
	});

	/*
	var mh = $('#mainContent').height();
	$('#mainContent').prepend('<div id="mainContentSlideShim" style="display:none"></div>');

	$('.bar-mainTitle .menu').on('click', function() {
		$('#mainContent').addClass('slid');
		$('#slideMenuContent').addClass('slid');

		$('#mainContentSlideShim').css({
			display: 'block',
			position: 'absolute',
			width: $('#mainContent').width(),
			height: $('#mainContent').height(),
			zIndex: 10000,
		});

		var h = $('#slideMenuContent').height();
		$('#mainContent').height(h);
	});

	$('#slideMenuContent .close, #mainContentSlideShim').on('click', function() {
		$('#mainContent').removeClass('slid');
		$('#slideMenuContent').removeClass('slid');
		$("#mainContentSlideShim").css('display', 'none');

		$('#mainContent').height(mh);
	});
	*/


	// accordion
	//$('.accordion .accordionContent').css('display', 'block');
	$('.accordion .accordionBtn').on('click', function() {
		/*
		$(this).next('.accordionContent').slideToggle(500, function(){
			$(this).prev('.accordionBtn').toggleClass('active');
		});
		*/

		if ( $(this).hasClass('active') ) {
			//alert('acrive');
			$(this).delay(500).queue(function() {
				$(this).removeClass('active').dequeue();
			});

			cookieManager.deleteCookie( 'slideAc' );

		} else {
			$(this).addClass('active');

			var ei = $('#slidemenu_list .accordion .accordionBtn').index(this);
			// alert(ei)

			if(ei != "-1"){
			cookieManager.setCookie( 'slideAc' , ei , 7 );
			}

		}

		$(this).next('.accordionContent').slideToggle(500);
	});

// window.onunload = function() {}

	//globalFunc.preload(['']);


	//$('body').prepend( $('<div class="skipNav"><a id="pagetop" name="pagetop">ページの先頭です</a></div>') );

	var toiawaseForm = $('#contact-baggage form');
	toiawaseForm.find('.submit a').click(function(){
		toiawaseForm.submit();
		return false;
	});


	var toggleContent = $('.toggleContent .textColumn');
	toggleContent.find('a').click(function(){
		$(this).toggleClass('on');
		var hideContent = $(this).parent().parent().next();
		hideContent.toggle();
		return false;
	});


	$('.toggleContent .toggle').each(function(){
		if( !$(this).hasClass('def-open')){
			$(this).hide();
		}else{
			$(this).parent().find('.bar-titleLink a').toggleClass('on');
		}
	});

	$('.bar-titleLink a').click(function(){
		$(this).toggleClass('on');
		var a = $(this).parent().next();
		a.toggle();
		return false;
	});


	$('.toggleContent .toggleBtn a').click(function(){
		$(this).toggleClass('on');
		var a = $(this).parent().next();
		a.toggle();

		 $(this).parent().parent().toggleClass('open');

		return false;
	});

// ハッシュクリック時
	$("a[href^=#].anc").click(function() {
		var href= jQuery(this).attr('href'), sty = $(href).closest("div.tabContent").attr('id'), ss = sty.slice(-1);
		if ($(href).closest("div.tabContent").is(":hidden")){
			$(".tab-link ul li a").removeClass('stay');
			$(".tabContent").hide();
			$(href).closest("div.tabContent").show();
			$(".tab-link ul li a").eq( ss -1 ).addClass('stay');
		}
	});


	$('a.anchor').click(function(){
		window.scrollTo(0,1);
		return false;
	});


	//to hash Scroll
/*
	if( location.hash ){
		setTimeout(function(){
			autoScroll(location.hash.replace('#',''))
		},200);
	}
*/


	$('.backBtn a').click(function(){
		history.back();
		return false;
	});

	var tabContent = $('.tabContent');
	/*
	tabContent.each(function(index,elm){
		if( index!=0 ){
			$(this).hide();
		}
	});*/

	var tabLink = $('.tab-link a');

	// タブクッキー

	tabLink.click(function(){
		var _this = $(this);
		var _href = _this.attr('href');
		var target = $(_href);
		var dir = location.href.split("/");
    var dir = dir[dir.length -2];
    var dir = "/" + dir;

		// $.cookie( dir , _href , { expires: 7 });
		cookieManager.setCookie( dir , _href , 7 );

		tabContent.hide();
		target.show();

		tabLink.removeClass('stay');
		_this.addClass('stay');

		return false;
	});

	$('.tab-link2 ul li').click(function() {
		var iii = $('.tab-link2 ul li').index(this) + 1;
		var tabC = '#tabContent0' + iii;
		var dir3 = location.href.split("/");
    	var dir3 = dir3[dir3.length -2];
    	var dir3 = "/" + dir3;
		// $.cookie( dir3 , tabC , { expires: 7 });
		cookieManager.setCookie( dir3 , tabC , 7 );
	});

	// 読み込み時に表示
	var lha = location.hash;
	var dir2 = location.href.split("/");
  var dir2 = dir2[dir2.length -2];
  var dir2 = "/" + dir2;
	var cookieTab = cookieManager.getCookie(dir2);
	var pnm = location.pathname;


if(lha){// ハッシュあり

	var tid = $(lha).closest("div.tabContent").attr('id');
	var ts = tid.slice(-1);

	// alert(ts)
	$(".tab-link ul li a").removeClass('stay');
	$(".tabContent").hide();
	$(lha).closest("div.tabContent").show();
	$(".tab-link ul li a").eq( ts -1 ).addClass('stay');
	setTimeout(function(){
			autoScroll(location.hash.replace('#',''))
	},1);

}else{// ハッシュ無し
	if ( cookieTab ){
		var ii = cookieTab.slice(-1) - 1 ;

		if ( pnm.indexOf('kuukou_02') == -1 ){

			$('.tab-link li a').removeClass("stay");
			$('.tabContent').hide();
			$('.tab-link li').eq(ii).find('a').addClass("stay");
			$(cookieTab).show();
		}

	}else{
		$('.tab-link .tab1').addClass("stay");
		$('#tabContent01').css('display','block');
	}
}


}


/*    Functions
-------------------------------------------------*/
function autoScroll(href){
	var _topClass = 'pageTop';
	var to = $('#'+href);
	if( to.length==0 ){
		if(href!=_topClass){ return false; }
	}
	var scrollTo = href==_topClass?1:to.offset().top;

	var _ua = navigator.userAgent.toLowerCase();
	//if( /isw[0-9]+ht/.test(_ua) ){
		//window.scroll( scrollTo, 0 );
	//}else{
		$('html,body').animate({ scrollTop: scrollTo }, 'slow', function(){
			ymtFlg = false;
		});
	//}
}

})();




/* 送り状番号 問い合わせ */
function win_Toi(){
	document.toi.action = "http://link.kuronekoyamato.co.jp/link/send/receive/lneko";
	//document.toi.target = "toiawase";
	document.toi.number01.value = document.toi.n01.value;
	document.toi.submit();
	//document.toi.n01.value = "";
	return false;
}



//-----------------------------------------------------------------
//
//		cookieManager
//
//-----------------------------------------------------------------
var cookieManager = {
	getExpDate : function(days, hours, minutes) {
		var expDate = new Date();
		if (typeof days == "number" && typeof hours == "number" && typeof minutes == "number") {
			expDate.setDate(expDate.getDate() + parseInt(days));
			expDate.setHours(expDate.getHours() + parseInt(hours));
			expDate.setMinutes(expDate.getMinutes() + parseInt(minutes));
			return expDate.toGMTString();
		}
	},
	setTommorow: function(){
		var dt = new Date();
		dt.setDate(dt.getDate() + 2);
		dt.setHours( - 15,00,00,00);
		return dt.toGMTString();
	},
	getCookieVal : function(offset) {
		var endstr = document.cookie.indexOf (";", offset);
		if (endstr == -1) {
			endstr = document.cookie.length;
		}
		return decodeURI(document.cookie.substring(offset, endstr));
	},
	getCookie : function(name) {
			var arg = name + "=";
			var alen = arg.length;
			var clen = document.cookie.length;
			var i = 0;
			while (i < clen) {
			var j = i + alen;
			if (document.cookie.substring(i, j) == arg) {
				return this.getCookieVal(j);
			}
			i = document.cookie.indexOf(" ", i) + 1;
				if (i == 0) break;
			}
		return "";
	},
	setCookie : function(name, value, expires, path, domain, secure) {
		document.cookie = name + "=" + encodeURI(value) +
		((expires) ? "; expires=" + expires : "") +
		((path) ? "; path=" + path : "") +
		((domain) ? "; domain=" + domain : "") +
		((secure) ? "; secure" : "");
	},
	deleteCookie : function(name,path,domain) {
		if (this.getCookie(name)) {
			document.cookie = name + "=" +
			((path) ? "; path=" + path : "") +
			((domain) ? "; domain=" + domain : "") +
			"; expires=Thu, 01-Jan-70 00:00:01 GMT";
		}
	}
};
