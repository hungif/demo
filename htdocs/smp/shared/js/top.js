/*    Global Function
-------------------------------------------------*/
var globalFunc = {};
globalFunc.preload = function (imgs){
	for(var i = 0; i < imgs.length; i++){
		var imgObj = new Image();
		imgObj.src = imgs[i];
	}
};
var ymtFlg = false;

(function(){

	$(function(){
		window.addEventListener('load', function(){
			main();
			setTimeout(function(){ if (window.pageYOffset === 0) { window.scrollTo(0,1); } }, 100);
		}, false);
	});

/*    main
-------------------------------------------------*/
function main(){

	var toiawaseForm = $('#contact-baggage form');
	toiawaseForm.find('.submit a').click(function(){
		toiawaseForm.submit();
		return false;
	});

	$('a.anchor').click(function(){
		window.scrollTo(0,1);
		return false;
	});
}

})();



