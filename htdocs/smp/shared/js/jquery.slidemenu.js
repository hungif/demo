(function($){
	var settings, status;
	var Status = {
		CLOSE: 0,
		OPEN: 1,
		IN_PROGRESS: 2,
		IN_FORM_FOCUS: 3,
	};
	var smY = 0, sfY, mfY, moveY, startY, draggedY, startTime, diffTime;


	function _isOldAndroid() {
		var ua = window.navigator.userAgent.toLowerCase();
		if (ua.indexOf('android 2') != -1) return true;
		return false;
	}

	function _open() {

		status = Status.OPEN;
		$(settings.menu + ", " + settings.main_contents).addClass('show');
		if (!_isOldAndroid()) {
			$(settings.main_contents + '.show').css({
				'-webkit-transform': 'translate3d(-260px, 0px, 0px)',
			});
		} else {
			$(settings.main_contents + '.show').css({
				'-webkit-transform': 'translate(-260px, 0px)',
			});
		}
		$(settings.menu_contents).show();

		// Add shim
		$(settings.main_contents).prepend('<div id="slidemenuShim"></div>')
		$('#slidemenuShim').css({
			display: 'block',
			position: 'absolute',
			zIndex: 1000,
			width: $(settings.main_contents).outerWidth(true),
			height: $(settings.main_contents).outerHeight(true),
		});



		$("html, body").css("overflow-x", "hidden");
			$("html").bind("touchmove.scrollContents", function() {
				event.preventDefault();
			});
			event.preventDefault();
	};

	function _close() {
		status = Status.IN_PROGRESS;
		if (!_isOldAndroid()) {
			$(settings.main_contents).css({
				'-webkit-transform': 'translate3d(0px, 0px, 0px)',
			});

			
		} else {
			$(settings.main_contents).css({
				'-webkit-transform': 'translate(0px, 0px)',
			});
		}
		$(settings.menu + ", " + settings.main_contents).removeClass('show');


		$('#slidemenuShim').remove();  // remove shim.
		$("html").unbind("touchmove.scrollContents");
		event.preventDefault();
	};

	function _buttonTouchStart() {
		switch(status) {
			case Status.IN_PROGRESS:
				status = Status.CLOSE;
				break;
			case Status.OPEN:
				_close();
				break;
			case Status.CLOSE:
				break;
		}
	}

	function _buttonTouchEnd() {
		switch(status) {
			case Status.IN_PROGRESS:
				status = Status.CLOSE;
				break;
			case Status.OPEN:
				break;
			case Status.CLOSE:
				_open();
				break;
		}
	};

	function _bodyTouchStart() {
		switch(status) {
			case Status.IN_PROGRESS:
			case Status.CLOSE:
				break;
			case Status.IN_FORM_FOCUS:
				event.preventDefault() /* リンク等がクリックされないように */
				break;
			case Status.OPEN:
				_close();
				break;
		}
	}

	$.fn.slideMenu = function(options) {
		settings = $.extend({}, $.fn.slideMenu.defaults, options);
		status = Status.CLOSE;
		var button_selector = this.selector;


		if (!_isOldAndroid()) {
			$(settings.wrapper).css({
				'-webkit-transform': 'translate3d(0px, 0px, 0px)',
				'-webkit-transition': '.2s -webkit-transform ease-in-out',
			});
			$(settings.main_contents).css({
				'-webkit-transform': 'translate3d(0px, 0px, 0px)',
				'-webkit-transition': '.2s -webkit-transform ease-in-out',
			});
		} else {
			$(settings.wrapper).css({
				'-webkit-transform': 'translate(0px, 0px)',
			});
			$(settings.main_contents).css({
				'-webkit-transform': 'translate(0px, 0px)',
			});
		}


		$(document).ready(function() {
			var menu_contents;
			
			/*
			$(button_selector).bind("click", function() {
				_buttonTouchStart();
			});
			$(button_selector).bind("click"  , function() {
				_buttonTouchEnd();
			});
			*/

			// form
			$("#searchNumTxtAside").focus(function () {
				status = Status.IN_FORM_FOCUS;
			});
			$("#searchNumTxtAside").blur(function () {
				status = Status.OPEN;
				$('body').scrollTop(0);
			});

			$('#closeBtn').bind("touchstart.menu_button", function() {
				_buttonTouchStart();
			});

			//close button - touchstart
			$('#closeBtn').bind("touchstart.menu_button", function() {
				_buttonTouchStart();
			});

			//menu button - touchstart
			$(button_selector).bind("touchstart.menu_button", function() {
				_buttonTouchStart();
			});

			//menu button - touchend
			$(button_selector).bind("touchend.menu_button"  , function() {
				_buttonTouchEnd();
			});

			//main contents
			$(settings.main_contents).bind("touchstart.main_contents", function() {
				_bodyTouchStart();
			});

			//scroll - touchStart
			$(settings.menu_contents).bind("touchstart.scrollMenu", function() {

				menu_contents = $(settings.menu_contents).height();
				sfY = event.touches[0].screenY; // タップした Y 座標
				startTime = (new Date()).getTime();
				startY = event.changedTouches[0].clientY;

			});

			//scroll - touchMove
			$(settings.menu_contents).bind("touchmove.scrollMenu", function() {

				//if (document.activeElement.id !== '') return false; // Form 選択時は疑似フリック無効 ※ 初回選択時はfireしない

				//if (status == Status.IN_FORM_FOCUS) return;

				endY = event.changedTouches[0].clientY
				if (startY != endY) {
					mfY = event.changedTouches[0].screenY;   // 移動した Y 座標
					moveY = smY + mfY - sfY;     //    移動 Y - タップ Y
					draggedY = event.changedTouches[0].clientY - startY;
					
					if (!_isOldAndroid()) {
						$(this).css({
							'-webkit-transition': 'none',
							'-webkit-transform': 'translate3d(0px,'+ moveY +'px,0px)',
						});
					} else {
						$(this).css({
							'-webkit-transform': 'translate(0px,'+ moveY +'px)',
						});
					}




				}

			});

			//scroll - touchEnd
			$(settings.menu_contents).bind("touchend.scrollMenu", function() {

				//if (document.activeElement.id !== '') return false; // Form 選択時は疑似フリック無効 ※ 初回選択時はfireしない
				//if (status !== Status.IN_FORM_FOCUS) {
					
				endY = event.changedTouches[0].clientY  // 終了した Y 座標
				diffTime = (new Date()).getTime() - startTime;

				// 慣性表現
				// タップ開始と終了座標が異なる場合
				if (startY != endY) {
					if (diffTime < 200 && draggedY > 0) { // scroll up
						moveY += Math.abs((draggedY / diffTime) * 500);
						if (!_isOldAndroid()) {
							$(settings.menu_contents).css({
								'-webkit-transition': '-webkit-transform .7s ease-out',
								'-webkit-transform': 'translate3d(0px,'+ moveY +'px,0px)',
							});
						} else {
							$(settings.menu_contents).css({
								'-webkit-transform': 'translate(0px,'+ moveY +'px)',
							});
						}

						smY = moveY;
					} else if (diffTime < 200 && draggedY < 0) { // scroll down
						moveY -= Math.abs((draggedY / diffTime) * 500);
						if (!_isOldAndroid()) {
							$(settings.menu_contents).css({
								'-webkit-transition': '-webkit-transform .7s ease-out',
								'-webkit-transform': 'translate3d(0px,'+ moveY +'px,0px)',
							});
						} else {
							$(settings.menu_contents).css({
								'-webkit-transform': 'translate(0px,'+ moveY +'px)',
							});
						}

						smY = moveY;
					}

					// 領域外バウンド表現
					if (moveY > 0) {
						// 制限上部より上、 0 位置にするだけ
						if (!_isOldAndroid()) {
							$(settings.menu_contents).css({
								'-webkit-transition': '-webkit-transform .5s ease-out',
								'-webkit-transform': 'translate3d(0px, '+ 0 +'px,0px)',
							});
						} else {
							$(settings.menu_contents).css({
								'-webkit-transform': 'translate(0px, '+ 0 +'px)',
							});
						}

						smY = 0;
					} else if (moveY < 0) {
						// 下に移動、処理
						var ajustY = moveY;
						if (menu_contents + moveY < $(window).height()) {
							ajustY = 0;
							if (menu_contents > $(window).height()) ajustY = $(window).height() - menu_contents;
							if (!_isOldAndroid()) {
								$(settings.menu_contents).css({
									'-webkit-transition': '-webkit-transform .5s ease-out',
									'-webkit-transform': 'translate3d(0px,' + ajustY + 'px,0px)',
								});
							} else {
								$(settings.menu_contents).css({
									'-webkit-transform': 'translate(0px,' + ajustY + 'px)',
								});
							}

						}
						smY = ajustY;
					}
				}
				//}
				
			});
		});
	};

	$.fn.slideMenu.defaults = {
		wrapper: '#wrapper',    /*  main_contents と menu の親要素 */
		main_contents: "#container",
		menu: "#slidemenu",
		menu_contents: "#slidemenu_contents",
		menu_list: "#slidemenu_list",
		speed: 200,
		width: 260,
		top_margin: 48,  // ヘッダー部分等の余白を取っている場合のピクセル数
	};


})(jQuery);