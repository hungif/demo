$(function(){
    /* toggle menu */
    $('#btn_toggle_menu').click(function(){
        $('.togglemenu_bg').height($('body').height()).fadeIn('fast');
        $('.togglemenu').slideDown(500);
    });
    $('.togglemenu a, .togglemenu_bg').click(function(){
        $('.togglemenu_bg').fadeOut('fast');
        $('.togglemenu').slideUp(500);
    });
    
    /* smooth scroll */
    $('a[href^=#]').click(function() {
        // スクロールの速度
        var speed = 400; // ミリ秒
        // アンカーの値取得
        var href= $(this).attr("href");
        // 移動先を取得
        var target = $(href == "#" || href == "" ? 'html' : href);
        // 移動先を数値で取得
        var position = target.offset().top;
        // スムーススクロール
        $('body,html').animate({scrollTop:position}, speed, 'swing');
        return false;
    });
    
    /* 画像反転 */
    $('.reverse').each(function(){
        $(this).bind('touchstart', function() {
            var src = $(this).attr('src');
            if($(this).attr('src').indexOf("_on.png") == -1){
                $(this).attr('src',src.replace(".png","_on.png"));
            }
        });
        $(this).bind('touchend', function() {
            var src = $(this).attr('src');
            $(this).attr('src',src.replace("_on.png",".png"));
        });
    });
    /* colorbox */
    $(".modalbox").click(function(){
        var h = $('body').height();
        var t = $(window).scrollTop()+10;
        var tgt = $(this).attr('href');
        var class_name = '';
        if($(tgt).hasClass('movie_box')){
            class_name = "movie_box";
        }
        $('body').append('<div id="bg_modalbox" style="display:none;height:'+h+'px;" onclick="modalbox.close()"><div class="btn_close" style="top:'+t+'px;">閉じる</div></div><div id="modalbox_body" class="'+class_name+'" style="display:none;top:'+(t+40)+'px;"><div class="modalbox_contents">'+$(tgt).html()+'</div></div>');
        $('#bg_modalbox, #modalbox_body').fadeIn(500);
    });

});

var modalbox = {
    'close' : function(){
        $('#bg_modalbox, #modalbox_body').fadeOut(500, function(){$(this).remove()});
    }
};


$(function () {

$("#blogToggle").hide();

  $("#blogtitle").click(function () {
    $("#blogToggle").slideToggle(700);
	$(this).toggleClass("active");
  });
  
});