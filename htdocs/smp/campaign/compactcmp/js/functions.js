(function ($) {
    $(document).ready(function () {
        window.test = $('.main').infiniteSlide({
            speed: 30,
            direction: 'right',
            pauseonhover: true
        });
        $('.blue').scrollTrigger({
            callback: function () {
                var $self = $(this);
                $self.css({
                    background: 'black'
                })
            },
            //top: 100
        });
        $(window).trigger('scroll');
    });
})(window.jQuery);
