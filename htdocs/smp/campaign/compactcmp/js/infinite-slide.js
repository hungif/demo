/**
 * Created by wushuyi on 2015/8/14.
 */
(function (global) {
    // Date.now
    if (!(Date.now && Date.prototype.getTime)) {
        Date.now = function now() {
            return new Date().getTime();
        };
    }

// performance.now
    if (!(global.performance && global.performance.now)) {
        var startTime = Date.now();
        if (!global.performance) {
            global.performance = {};
        }
        global.performance.now = function () {
            return Date.now() - startTime;
        };
    }

// requestAnimationFrame
    var lastTime = Date.now();
    var vendors = ['ms', 'moz', 'webkit', 'o'];

    for (var x = 0; x < vendors.length && !global.requestAnimationFrame; ++x) {
        global.requestAnimationFrame = global[vendors[x] + 'RequestAnimationFrame'];
        global.cancelAnimationFrame = global[vendors[x] + 'CancelAnimationFrame'] ||
            global[vendors[x] + 'CancelRequestAnimationFrame'];
    }

    if (!global.requestAnimationFrame) {
        global.requestAnimationFrame = function (callback) {
            if (typeof callback !== 'function') {
                throw new TypeError(callback + 'is not a function');
            }

            var currentTime = Date.now(),
                delay = 16 + lastTime - currentTime;

            if (delay < 0) {
                delay = 0;
            }

            lastTime = currentTime;

            return setTimeout(function () {
                lastTime = Date.now();
                callback(performance.now());
            }, delay);
        };
    }

    if (!global.cancelAnimationFrame) {
        global.cancelAnimationFrame = function (id) {
            clearTimeout(id);
        };
    }

})(window);


(function (win, $) {
    var anim = $({});
    var ticker = function () {
        win.requestAnimationFrame(function () {
            anim.trigger('tick');
            ticker();
        });
    };
    ticker();

    $.fn.infiniteSlide = function (options) {
        var $self = $(this);
        var $slideBox = $self.children();
        var slideBox = $slideBox.get(0);
        var opts = $.extend({}, $self.infiniteSlide.defaultOpts, options);

        var toLeft;
        if (opts.direction === 'left') {
            toLeft = true;
        } else {
            toLeft = false;
        }
        var childrenWidth = $slideBox.width();
        var $slideClone = $slideBox.children().clone();
        $slideBox.append($slideClone);
        $slideBox.width(childrenWidth * 2);
        var step = 0;
        var speed = Math.floor(60 * opts.speed);
        var stepLength = childrenWidth / speed;

        if (!toLeft) {
            $slideBox.css('margin-left', -childrenWidth);
        }
        var run = function run() {
            toLeft ? step-- : step++;
            var nowLeft = step * stepLength;
            if (Math.abs(step) === speed) {
                step = 0;
            }
            slideBox.style.left = Math.floor(nowLeft) + 'px';
        };

        anim.on('tick', run);
        if (opts.pauseonhover) {
            $self.on('mouseenter.infiniteSlide', function () {
                anim.off('tick');
            });
            $self.on('mouseleave.infiniteSlide', function () {
                anim.on('tick', run);
            });
        }

        var distory = function distory() {
            $self.off('.infiniteSlide');
            anim.off('tick');
            $slideClone.remove();
            $slideBox.attr('style', '')
        };

        return {
            distory: distory
        };
    };

    $.fn.infiniteSlide.defaultOpts = {
        speed: 30,
        direction: 'left',
        pauseonhover: true
    };

})(window, jQuery);