(function(w,$){
  $(function(){

    // arrow animation
    var arrow = $(".js-down-arrow");
    leafmove(arrow);

    // smooth anchor link
    setanchoranimate();
  });


  // --------------------------------
  // functions
  // --------------------------------
  function isEven(num){
    if (typeof num !== 'number') throw new Error("num is number!!");
    return num % 2 === 0;
  }

  function leafmove(ele){

    var cyclesec = 800;
    var startposy = parseInt(ele.css("top"), 10);
    var downposy = startposy + 10;
    var count = 0;

    var leafTimer = setInterval(leaf, cyclesec);

    function leaf(){
      var posy = isEven(count) ? downposy : startposy;

      ele.animate({top: posy}, cyclesec, "swing");
      count++;
    }

  }

  function moveanchor(ele) {
    var animateParam, easing, href, speed, target;

    href = ele.attr("href");
    speed = 500;
    easing = 'swing';
    target = $((href === "#" || href === "" ? "html" : href));
    animateParam = {
      scrollTop: target.offset().top
    };

    $("html, body").animate(animateParam, speed, easing);
  }

  function setanchoranimate() {
    notEle = "a[href=#], .js-noAnchor";
    $("a[href^=#]").not(notEle).on("click", function(){ moveanchor($(this)); });
    return;
  }


})(window, jQuery);
