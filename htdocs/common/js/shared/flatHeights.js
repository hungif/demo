
/* Jquery Flat Heights
==========================================
Version: 2007-08-01

Copyright (c) 2007, KITAMURA Akatsuki

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
====================================================================== */
/*
	jQuery.changeLetterSize.addHandler(func)
	文字の大きさが変化した時に実行する処理を追加
*/

jQuery.changeLetterSize = {
	handlers : [],
	interval : 500,//定期処理を行う時間間隔。一応0.5秒で設定して有ります。1秒 = 1000
	currentSize: 0
};

(function(jQuery) {

	var self = jQuery.changeLetterSize;

	/* 文字の大きさを確認するためのins要素 */
	var ins = jQuery('<ins>M</ins>').css({
		display: 'block',
		visibility: 'hidden',
		position: 'absolute',
		padding: '0',
		top: '0'
	});

	/* 文字の大きさが変わったか */
	var isChanged = function() {
		
		//ins.appendTo('body');
		
		/*文字サイズ変更ボタン押下時、
		#wrapper以下のフォントサイズが変更となるため、
		判定用要素のappend先を変更*/
		
		ins.appendTo('#wrapper');
		var size = ins[0].offsetHeight;
		ins.remove();

		if (heightFlag)
		{
			heightFlag = false;
			return true;
		}
		else if (self.currentSize == size) return false;
		self.currentSize = size;
		
		return true;
	};

	/* 文書を読み込んだ時点で
	   文字の大きさを確認しておく */
	jQuery(isChanged);

	/* 文字の大きさが変わっていたら、
	   handlers中の関数を順に実行 */
	var observer = function() {
		if (!isChanged()) return;
		
		jQuery.each(self.handlers, function(i, handler) {
			handler();
		});
	};

	/* ハンドラを登録し、
	   最初の登録であれば、定期処理を開始 */
	self.addHandler = function(func) {
		self.handlers.push(func);
		if (self.handlers.length == 1) {
			setInterval(observer, self.interval);
		}
	};

})(jQuery);

/*
	jQuery(expr).flatHeights()
	jQuery(expr)で選択した複数の要素について、それぞれ高さを
	一番高いものに揃える
*/

(function(jQuery) {

	/* 対象となる要素群の集合 */
	var sets = [];

	/* 高さ揃えの処理本体 */
	var flatHeights = function(set) {
		var maxHeight = 0;
		set.each(function(){
			var height = this.offsetHeight;
			if (height > maxHeight) maxHeight = height;
		});
		set.css('height', maxHeight + 'px');
	};

	/* 要素群の高さを揃え、setsに追加 */
	jQuery.fn.flatHeights = function() {
		if (this.length > 1) {
			flatHeights(this);
			sets.push(this);
		}
		return this;
	};

	/* 文字の大きさが変わった時に、
	   setsに含まれる各要素群に対して高さ揃えを実行 */
	jQuery.changeLetterSize.addHandler(function() {
		jQuery.each(sets, function() {
			this.height('auto');
			flatHeights(this);
		});
	});

})(jQuery);


	/* 2-1.Setting
	---------------------------------------- */
	jQuery(function(){
		jQuery('.tabList li').flatHeights();
		jQuery('.tabHead li').flatHeights();
		jQuery('.boxHeight').flatHeights();
		jQuery('.boxHeight01').flatHeights();
		jQuery('.boxHeight02').flatHeights();
		jQuery('.boxHeight03').flatHeights();
		jQuery('.boxHeight04').flatHeights();
		jQuery('.boxHeight05').flatHeights();
		jQuery('.sendHeght01').flatHeights();
		jQuery('.sendHeght02').flatHeights();
		jQuery('.sendHeght03').flatHeights();
		jQuery('.sendHeght04').flatHeights();
		jQuery('.secCont4clm .inner').flatHeights();
		jQuery('.secCont4clm h2').flatHeights();
		
		$(".tabHead").each(function(){
			$(this).children("li").css("background-position","top left");
			$(this).children("li").css("color","#333");
			$(this).children("li").eq(num).addClass("tabCurrent");
			$(this).children("li").eq(num).css("background-position","top right");
			$(this).children("li").eq(num).css("color","#fff");
			$(this).children("li").eq(3).css("margin-right","0px");
		});
		
	});

	/* 2Column
	---------------------------------------- */

	$(function(){
	/* div要素を2つずつの組に分ける */
	var sets = [], temp = [];
	$('.ContentsArea-2col .ContentsBtm > div').each(function(i) {
		temp.push(this);
		if (i % 2 == 1) {
			sets.push(temp);
			temp = [];
		}
	});
	if (temp.length) sets.push(temp);

	/* 各組ごとに高さ揃え */
	$.each(sets, function() {
		$(this).flatHeights();
	});
});

	/* 2Column h2
	---------------------------------------- */

	$(function(){
	/* a要素を2つずつの組に分ける */
	var sets = [], temp = [];
	$('.ContentsArea-2col h2 > a').each(function(i) {
		temp.push(this);
		if (i % 2 == 1) {
			sets.push(temp);
			temp = [];
		}
	});
	if (temp.length) sets.push(temp);

	/* 各組ごとに高さ揃え */
	$.each(sets, function() {
		$(this).flatHeights();
	});
});

	/* serviceType
	---------------------------------------- */
	$(function(){
		$('.ContentsLeft .ContentsInner, .ContentsRight .ContentsInner').flatHeights();
	});
	
	/* Q&A
	---------------------------------------- */
	$(function(){
	/* li要素を2つずつの組に分ける */
	var sets = [], temp = [];
	$('.contactPageLink_qa > li').each(function(i) {
		temp.push(this);
		if (i % 2 == 1) {
			sets.push(temp);
			temp = [];
		}
	});
	if (temp.length) sets.push(temp);

	/* 各組ごとに高さ揃え */
	$.each(sets, function() {
		$(this).flatHeights();
	});
});

/* Tab
	---------------------------------------- */
	$(function(){
   		$('.tabHead2col li, .tabHead2col li a').flatHeights();
	});
	
	$(function(){
   		$('.tabHead3col li, .tabHead3col li a').flatHeights();
	});

	$(function(){
   		$('.tabHead4col li, .tabHead4col li a').flatHeights();
	});