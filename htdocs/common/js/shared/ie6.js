/*背景画像用*/
try {
document.execCommand('BackgroundImageCache', false, true);
} catch(e) {}// JavaScript Document
/* PNG用 */
DD_belatedPNG.fix('.png');
DD_belatedPNG.fix('h2');
DD_belatedPNG.fix('ul.gnaviPulldown li ul');
DD_belatedPNG.fix('.loginBoxIn');
DD_belatedPNG.fix('.loginBoxBizIn');
DD_belatedPNG.fix('.asideMemb img');
DD_belatedPNG.fix('#footImg');
DD_belatedPNG.fix('.tabsectionCont');
DD_belatedPNG.fix('.tabBody');
DD_belatedPNG.fix('.newsTtlBold');
DD_belatedPNG.fix('ul.tabHead li');

/**
 * IE6以下用csshover
 */
(function(win){
	if (!document.all)return;
	var rules = {};
	var inithitstyle1 = /(^|\s)((([^a]([^ ]+)?)|(a([^#.][^ ]+)+)):(hover|active|focus))/i;
	var inithitstyle2 = /(.*?)\:(hover|active|focus)/i;
	var initreg_newSelect = /(\.([a-z0-9_\-]+):[a-z]+)|(:[a-z]+)/gi;
	var initreg_className = /\.([a-z0-9_\-]*on(hover|active|focus))/i;
	var indexProperties = 0;
	var listProperties = ['text-kashida', 'text-kashida-space', 'text-justify'];
	var Properties = function () {
		return listProperties[(indexProperties++)%listProperties.length];
	};

	function camelize(str) {
		return str.replace(/-(.)/mg, function(result, match){
			return match.toUpperCase();
		});
	};

	/**
	 * hover初期化
	 * @private
	 */
	function init(sheet) {
		if(!sheet) {
			var sheets = window.document.styleSheets;
			for(var i=0; i<sheets.length; i++) {
				init(sheets[i]);
			}
			return;
		}
		if(sheet.imports && 0<sheet.imports.length) {
			var imports_len = sheet.imports.length;
			for(var i=0; i<imports_len; i++) {
				init(sheet.imports[i]);
			}
		}
		try {
			var rules_len = sheet.rules.length;
			for(var i=0; i<rules_len; i++) {
				initRule(sheet, i);
			}
		} catch(e){}
	}

	/**
	 * hover用スタイルシートを初期化
	 * @private
	 */
	function initRule(sheet, index) {
		try {
			var rule = sheet.rules[index];
			var select = rule.selectorText
			var style = rule.style.cssText;
			if(style && inithitstyle1.test(select)) {
				var hits = inithitstyle2.exec(select);
				if (hits) {
					var key = hits[1];
					var mode = hits[2];
					var newSelect = select.replace(initreg_newSelect, '.$2on' + mode);
					var className = (initreg_className).exec(newSelect)[1];
					var hash = key + className;
					if(!rules[hash]) {
						var property = Properties();
						var atRuntime = camelize(property);
						sheet.addRule(key, property + ':expression(csshover(this, "'+mode+'", "'+className+'", "'+atRuntime+'"))');
						rules[hash] = true;
					}
					sheet.addRule(newSelect, style);
				}
			}
		} catch(e){}

	}
	
	function addClass(node, className) {
		var clsnms = node.className.split(' ');
		for(var i=0;i<clsnms.length;i++) if (clsnms[i] == className) return;
		node.className += ' '+className;
	}
	
	function removeClass(node, className) {
		var clsnms = node.className.split(' ');
		var i=0;
		while(i<clsnms.length) if (clsnms[i] == className) clsnms.splice(i, 1);else i++;
		node.className = clsnms.join(' ');
	}
	
	/**
	 * hoverイベント追加
	 * @private
	 */
	function addEvent(node, mode, className) {
		var h_on  = function() { addClass(node, className); };
		var h_off = function() { removeClass(node, className); }
		switch(mode) {
			case 'hover' : var on = 'onmouseenter';var off = 'onmouseleave';break;
			case 'active': var on = 'onmousedown';var off = 'onmouseup';break;
			case 'focus': var on = 'onfocus';var off = 'onblur';break;
		}
		node.attachEvent(on, h_on);
		node.attachEvent(off, h_off);
	}
	
	/**
	 * イベントを登録する
	 */
	win.csshover = function(node, type, className, property) {
		try {
			var value = "none"==node.parentNode.currentStyle.display ? "" : node.parentNode.currentStyle[property];
			node.style[property] = value;	
		} catch(e) {
			node.runtimeStyle[property] = '';
		}			
		if(!node.csshover) {
			node.csshover = [];
		}
		if(!node.csshover[className]) {
			node.csshover[className] = true;
			addEvent(node, type, className);
		}
		return type;
	}
	
	/**
	 * hover適用
	 */
	init();
})(window);