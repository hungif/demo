/* 
ブラウザ判定　04/05/24

*/

/* user agent */
var _agent = navigator.userAgent.toLowerCase();
var _an    = navigator.appName;
var _ua    = navigator.userAgent;

/* platform */
var _win = (_agent.indexOf('win') != -1);
var _mac = (_agent.indexOf('mac') != -1);

/* browser version */
var _major = parseInt(navigator.appVersion);
var _minor = parseFloat(navigator.appVersion);

/* internet explorer */
var _ie    = (_agent.indexOf('msie ') != -1);
var _ie3   = (_ie && (_major < 4));
var _ie4   = (_ie && (_major == 4) && (_agent.indexOf('msie 5') == -1));
var _ie4up = (_ie && (_major >= 4));
var _ie45  = (_ie && (_minor == 4.5));
var _ie5   = (_ie && (_major == 4) && (_agent.indexOf('msie 5') != -1));
var _ie51  = (_ie && (_major == 4) && (_agent.indexOf('msie 5.1') != -1));
var _ie5up = (_ie && !_ie3 && !_ie4);
var _ie6   = (_ie && (_major == 4) && (_agent.indexOf('msie 6') != -1));

/* netscape */
var _ns    = ((_agent.indexOf('mozilla') != -1) && (_agent.indexOf('compatible') == -1));
var _ns2   = (_ns && (_major == 2));
var _ns3   = (_ns && (_major == 3));
var _nav   = (_ns && ((_agent.indexOf(';nav') != -1) || (_agent.indexOf('; nav') != -1)));
var _nav3  = (_ns && _nav && (_major == 3));
var _nav4  = (_ns && _nav && (_major == 4));
var _ns4   = (_ns && (_major == 4));
var _ns4up = (_ns && (_major >= 4));
var _ns6   = (_ns && (_major == 5));
var _ns6up = (_ns && (_major >= 5));
var _ns61  = (_ns6 && (_agent.indexOf('netscape6/6.1') != -1));
var _ns62  = (_ns6 && (_agent.indexOf('netscape6/6.2') != -1));
var _ns7   = (_agent.indexOf('netscape/7') != -1);

/* safari */
var _saf   = (_ua.indexOf("Safari") != -1);

/* mozilla */
var _moz   = (_ns6 && (_agent.indexOf('netscape') == -1));

/* opera */
var _opera  = (_agent.indexOf('opera') != -1);
var _opera5 = (_opera && (_major == 5));
var _opera6 = (_opera && (_major == 6));
var _opera7 = (_opera && (_major == 7));

/* Mac OS　判定 */
var _mac_os10_ie = (_ua.indexOf('MSIE 5.2') != -1) || ((_ua.indexOf('mozilla') != -1) && (_ua.indexOf('Mac OS X') != -1));
var _mac_os10_ns = (_ua.indexOf('Mac OS X') != -1);

/* 各ブラウザを判別しての分岐
   Opera、MozillaはそれぞれIE6、NN6と判別してしまう可能性があるので、先に記述する
*/


/* Win用 */
if
(_win && _ie)
{document.write('<LINK rel="stylesheet" href="../../../../common/css/win_ie.css" type="text/css">');}

else if
(_win && _ns6)
{document.write('<LINK rel="stylesheet" href="../../../../common/css/win_nn6.css" type="text/css">');}

else if
(_win && _ns4)
{document.write('<LINK rel="stylesheet" href="../../../../common/css/win_nn4.css" type="text/css">');}


/* Mac用 */
else if
(_saf)
{document.write('<LINK rel="stylesheet" href="../../../../common/css/mac_saf.css" type="text/css">');}

else if
(_mac && _ie)
{document.write('<LINK rel="stylesheet" href="../../../../common/css/mac_ie.css" type="text/css">');}

else if
(_mac && _ns6)
{document.write('<LINK rel="stylesheet" href="../../../../common/css/mac_nn6.css" type="text/css">');}

else if
(_mac && _ns4)
{document.write('<LINK rel="stylesheet" href="../../../../common/css/mac_nn4.css" type="text/css">');}

else
{document.write('<LINK rel="stylesheet" href="../../../../common/css/win_ie.css" type="text/css">');}