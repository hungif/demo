/* 「計算機能（タイヤを送りたい）｜ひまわり」から引用 */

		//変数の宣言
		 var width;
		 var flat;
		 var radial;
		 var length;
		 var height;
		 var weightOne;
		 var weightTwo;
		 var weightThree;
		 var weightFour;
		 var referOne;
		 var referTwo;
		 var referThree;
		 var referFour;
		 var total;
		 var totalTwo;



		function keisan(){

			Judg = false;
			var strlength = document.form1.WidthSection.value;
			if( strlength.match( /[^0-9]+/ ) ) {
				alert("断面幅は半角数字のみで入力して下さい。");
				document.form1.WidthSection.value="";
				allClear();
				document.getElementById('firstInput').focus();
				return 1;
			}

			var strwidth = document.form1.FlatRate.value;
			if( strwidth.match( /[^0-9]+/ ) ) {
				alert("偏平率は半角数字のみで入力して下さい。");
				document.form1.FlatRate.value="";
				allClear();
				document.getElementById('secondInput').focus();
				return 1;
			}

			var strheight = document.form1.Radial.value;
			if( strheight.match( /[^0-9]+/ ) ) {
				alert("インチ(R)は半角数字のみで入力して下さい。");
				document.form1.Radial.value="";
				allClear();
				document.getElementById('thirdInput').focus();
				return 1;
			}


			width=document.form1.WidthSection.value;
			flat=document.form1.FlatRate.value;
			radial=document.form1.Radial.value;


			if(width==""){
				alert("断面幅を入力して下さい。");
				Judg = true;
				document.getElementById('firstInput').focus();
			}else if(width>1000){
				alert("断面幅は1000以下で入力して下さい。");
				Judg = true;
				document.getElementById('firstInput').focus();
			}else if(width==0){
				alert("断面幅は1以上で入力して下さい。");
				Judg = true;
				document.form1.WidthSection.value="";
				document.getElementById('firstInput').focus();
			}else if(flat==""){
				alert("偏平率を入力して下さい。");
				Judg = true;
				document.getElementById('secondInput').focus();
			}else if(flat>100){
				alert("偏平率は100以下で入力して下さい。");
				Judg = true;
				document.getElementById('secondInput').focus();
			}else if(flat==0){
				alert("偏平率は1以上で入力して下さい。");
				document.form1.FlatRate.value="";
				Judg = true;
				document.getElementById('secondInput').focus();
			}else if(radial==""){
				alert("インチ(R)を入力して下さい。");
				Judg = true;
				document.getElementById('thirdInput').focus();
			}else if(radial>100){
				alert("インチ(R)は100以下で入力して下さい。");
				Judg = true;
				document.getElementById('thirdInput').focus();
			}else if(radial==0){
				alert("インチは1以上で入力して下さい。");
				Judg = true;
				document.form1.Radial.value="";
				document.getElementById('thirdInput').focus();
			}else{

				//縦・横の計算
				length = (radial * 2.54 + (width/10) * (flat/100) *2)/100;
	
				//高さの計算
				height = width/1000;
	
				//一本の場合の重量換算結果
				weightOne = length * length * height * 280;
				
				//一本の場合の重量換算結果の表示
				document.form1.Length.value=(Math.round((length*100)*10))/10;
				document.form1.Side.value=(Math.round((length*100)*10))/10;
				document.form1.Height.value=(Math.round((height*100)*10))/10;
				total = (Math.round((length*100*2 + height*100)*10))/10;
				totalTwo = (Math.round((length*100*2 + height*100+height*100)*10))/10;
	
				//宅急便で送れるか判断
				if(total<=160&&total>0){
					document.form1.textfield1.value = "宅急便規格内。実重量を確認して25kg以内の場合は宅急便運賃表を、超える場合はヤマト便運賃表をご覧ください。";
				}else if(total>160){
					document.form1.textfield1.value = "宅急便規格外。実重量を確認してヤマト便運賃表をご覧ください。";
				}
	
				if(totalTwo<=160&&totalTwo>0){
					document.form1.textfield2.value = "宅急便規格内。実重量を確認して25kg以内の場合は宅急便運賃表を、超える場合はヤマト便運賃表をご覧ください。";
				}else if(totalTwo>160){
					document.form1.textfield2.value = "宅急便規格外。実重量を確認してヤマト便運賃表をご覧ください。";
				}
				
				
				//二本の場合の重量換算結果
				weightTwo = (Math.round(weightOne * 2 *100))/100;

				//三本の場合の重量換算結果
				weightThree = (Math.round(weightOne * 3 *100))/100;

				//四本の場合の重量換算結果
				weightFour =(Math.round(weightOne* 4 *100))/100 ;

				document.form1.Total.value = total;
				document.form1.TotalTwo.value = totalTwo;
	
				weightOneMethod();

			}

			if(Judg){
				document.form1.YamatoWeightOne.value="";
				document.form1.YamatoWeightTwo.value="";
				document.form1.YamatoWeightThree.value="";
				document.form1.YamatoWeightFour.value="";
				document.form1.Length.value="";
				document.form1.Side.value="";
				document.form1.Height.value="";
				document.form1.Total.value="";
				document.form1.textfield1.value="";
				document.form1.TotalTwo.value="";
				document.form1.textfield2.value="";
			}
		}

		//ヤマト便重量計算
		function weightOneMethod(){

			if(weightOne <= 100){
				if(weightOne<=30){
					document.form1.YamatoWeightOne.value=30;
				}else if((weightOne > 30) && (weightOne <= 40)){
					document.form1.YamatoWeightOne.value=40;
				}else if((weightOne >40) && (weightOne <= 60)){
					document.form1.YamatoWeightOne.value=60;
				}else if((weightOne >60) && (weightOne <= 80)){
					document.form1.YamatoWeightOne.value=80;
				}else if((weightOne >80) && (weightOne <= 100)){
					document.form1.YamatoWeightOne.value=100;
				}
			} else {
				loop1:
				for(referOne = 100;referOne <= weightOne;){
					referOne = referOne + 20;
				}
				document.form1.YamatoWeightOne.value=referOne;
			}

			if(weightTwo <= 100){
				if(weightTwo<=30){
					document.form1.YamatoWeightTwo.value=30;
				}else if((weightTwo > 30) && (weightTwo <= 40)){
					document.form1.YamatoWeightTwo.value=40;
				}else if((weightTwo >40) && (weightTwo <= 60)){
					document.form1.YamatoWeightTwo.value=60;
				}else if((weightTwo >60) && (weightTwo <= 80)){
					document.form1.YamatoWeightTwo.value=80;
				}else if((weightTwo >80) && (weightTwo <= 100)){
					document.form1.YamatoWeightTwo.value=100;
				}
			} else {
				loop2:
				for(referTwo = 100;referTwo < weightTwo;){
					referTwo = referTwo + 20;
				}
				document.form1.YamatoWeightTwo.value=referTwo;
			}

			if(weightThree <= 100){
				if(weightThree<=30){
					document.form1.YamatoWeightThree.value=30;
				}else if((weightThree > 30) && (weightThree <= 40)){
					document.form1.YamatoWeightThree.value=40;
				}else if((weightThree >40) && (weightThree <= 60)){
					document.form1.YamatoWeightThree.value=60;
				}else if((weightThree >60) && (weightThree <= 80)){
					document.form1.YamatoWeightThree.value=80;
				}else if((weightThree >80) && (weightThree <= 100)){
					document.form1.YamatoWeightThree.value=100;
				}
			} else {
				loop3:
				for(referThree = 100;referThree <= weightThree;){
					referThree = referThree + 20;
				}
				document.form1.YamatoWeightThree.value=referThree;
			}

			if(weightFour <= 100){
				if(weightFour<=30){
					document.form1.YamatoWeightFour.value=30;
				}else if((weightFour > 30) && (weightFour <= 40)){
					document.form1.YamatoWeightFour.value=40;
				}else if((weightFour >40) && (weightFour <= 60)){
					document.form1.YamatoWeightFour.value=60;
				}else if((weightFour >60) && (weightFour <= 80)){
					document.form1.YamatoWeightFour.value=80;
				}else if((weightFour >80) && (weightFour <= 100)){
					document.form1.YamatoWeightFour.value=100;
				}
			} else {
				loop4:
				for(referFour =100;referFour <= weightFour;){
					referFour = referFour + 20;
				}
				document.form1.YamatoWeightFour.value=referFour;
			}
		}
		
		function allClear(){
			document.form1.YamatoWeightOne.value="";
			document.form1.YamatoWeightTwo.value="";
			document.form1.YamatoWeightThree.value="";
			document.form1.YamatoWeightFour.value="";
			document.form1.Length.value="";
			document.form1.Side.value="";
			document.form1.Height.value="";
			document.form1.Total.value="";
			document.form1.textfield1.value="";
			document.form1.TotalTwo.value="";
			document.form1.textfield2.value="";
		}
