$(function(){
  $("#toggle a").click(function(){
    $(".nav").slideToggle();
    return false;
  });
  $(window).resize(function(){
    var win = $(window).width();
    if(win > 640){
      $(".nav").show();
    } else {
      $(".nav").hide();
      var userAgent = window.navigator.userAgent.toLowerCase();
      var appVersion = window.navigator.appVersion.toLowerCase();
      if (userAgent.indexOf("msie") != -1) {
        if (appVersion.indexOf("msie 8.0")  != -1) {
          $(".nav").show();
        }
      }
    }
  });
});

$(function(){
  $(window).scroll(function(){
    if($(window).scrollTop() > 600){
      $('#pagetop').fadeIn('slow');
    }else{
      $('#pagetop').fadeOut('slow');
    }
  });
  $('#pagetop img').click(function(){
    $('body,html').animate({
            scrollTop: 0
    }, 800);
    return false;
  });
});

/* smooth scroll */
$(function(){
   $('a[href^="#"]').click(function() {
      var speed = 400;
      var href= $(this).attr("href");
      var target = $(href == "#" || href == "" ? 'html' : href);
      var position = target.offset().top;
      $('body,html').animate({scrollTop:position}, speed, 'swing');
      //return false;
   });
});
