// ポップアップウィンドウを開く
var openWin = window;
function openWindow(OpenWinURL,w,h) {
		if ((openWin == window) || openWin.closed) {
		  openWin = open(OpenWinURL, "_newWindow", "location=no,directories=no,resizable=yes,menubar=yes,scrollbars=yes,width="+w+",height="+h);
   } else {
      openWin.focus();
   }
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

// セレクトメニューからの移動
function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}

function MM_jumpMenu(targ,selObj,restore){ //v3.0
  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}

function MM_jumpMenuGo(selName,targ,restore){ //v3.0
  var selObj = MM_findObj(selName); if (selObj) MM_jumpMenu(targ,selObj,restore);
}

// 横スクロール
var now = 0;
var stp = 5;
brName = navigator.appName;
brVer = parseInt(navigator.appVersion);
var apV;
var nowPos;
var mov_LEFTID = false;
var mov_RIGHTID = false;

if (brName == "Netscape" && brVer >= 4) apV = "n4";
else if (brName == "Netscape" && brVer >= 3) apV = "n3";
else if (brName == "Netscape" && brVer == 2) apV = "n2";
else if (brName == "Microsoft Internet Explorer" && brVer >= 2 && brVer <= 3) apV = "e3";
else if (brName == "Microsoft Internet Explorer" && brVer >= 4) apV = "e4";

function slideMe_left(clPos,n) {
now += Math.ceil((n-now)*0.07);
self.scroll(now,0);
if ((n-now) <= 1) {
now = n;
self.scroll(n,0);
clearTimeout(mov_LEFTID);
clearTimeout(mov_RIGHTID);
clearTimeout(mov_STID);
clearAll();
} else mov_LEFTID = setTimeout ("slideMe_left(" + clPos + "," + n + ")",0);
}

function slideMe_right(clPos,n) {
now -= Math.ceil((now-n)*0.07);
self.scroll(now,0);
if (Math.abs(n-now) <= 1) {
now = n;
self.scroll(n,0);
clearTimeout(mov_LEFTID);
clearTimeout(mov_RIGHTID);
clearTimeout(mov_STID);
clearAll();
} else mov_RIGHTID = setTimeout ("slideMe_right(" + clPos + "," + n + ")",0);
}

function slidePage(bool,clPos,n) {
if (!mov_LEFTID && !mov_RIGHTID) {
if (now > n && bool) now = clPos;
else if (now < n && !bool) now = clPos;
if (apV == "e5") {
self.scroll(n,0);
now = n;
} else {
if (bool) {
mov_STID = setTimeout ("slideMe_left(" + clPos + "," + n + ")",0);
} else {
mov_STID = setTimeout ("slideMe_right(" + clPos + "," + n + ")",0);
}
}
}
}

function clearAll() {
mov_LEFTID = false;
mov_RIGHTID = false;
mov_STID = false;
}


// ロールオーバーイメージ
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

