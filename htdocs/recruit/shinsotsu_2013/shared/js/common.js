$(function(){
	var postfix = '_on';
	$('#globalNav a').children('img').not('[src*="'+ postfix +'."]').each(function() {
		var img = $(this);
		var src = img.attr('src');
		var src_on = src.substr(0, src.lastIndexOf('.')).replace('_off', '')
		           + postfix
		           + src.substring(src.lastIndexOf('.'));
		$('<img>').attr('src', src_on);
		img.hover(function() {
			img.attr('src', src_on);
		}, function() {
			img.attr('src', src);
		});
	});

	$('.pageTop').click(function(){
		$('html, body').animate({scrollTop:0}, {easing:'easeInQuart', duration:800});
		return false;
	});

	$('#questionNav li a').click(function(){
		var targetName = $(this).attr('href');
		var targetPos = $(targetName).offset().top;
		$('html, body').animate({scrollTop:targetPos}, {easing:'easeInQuart', duration:800});
		return false;
	});
});