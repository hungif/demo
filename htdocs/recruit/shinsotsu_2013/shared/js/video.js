$(document).ready(function(){
	var urlVal = location.search;
	urlVal = urlVal.substring(1, urlVal.length);
	var userAgent = window.navigator.userAgent.toLowerCase();
	var appVersion = window.navigator.appVersion.toLowerCase();
	var movieHeight = 600;

	if (appVersion.indexOf("msie 6.") != -1 || appVersion.indexOf("msie 7.") != -1) {
		movieHeight = 618;
	}

	if (navigator.userAgent.indexOf('iPhone') > 0 || navigator.userAgent.indexOf('iPad') > 0 || navigator.userAgent.indexOf('iPod') > 0 || navigator.userAgent.indexOf('Android') > 0) {
		$('#playBtn').attr('id', 'playVideo');
		$('#videoImage').remove();
		MediaElement('movie', {success: function(me) {
				document.getElementById('playVideo')['onclick'] = function() {
					if (me.paused) {
						me.play();
					} else {
						me.pause();
					}
				};
				if (urlVal=='play') {
					me.play();
				}
			}
		});
	} else {
		$('#movieArea').remove();
		if (urlVal=='play') {
			$.colorbox({iframe:true, innerWidth:800, innerHeight:movieHeight, 'href':'movie.html'});
		}
	}

	$('#playBtn').click(function(){
		$(this).colorbox({iframe:true, innerWidth:800, innerHeight:movieHeight});
	});
});
