/*-------------------------------------------------------------
	Yamato Transport 採用情報  ver1.1
	designed by common-design
-------------------------------------------------------------*/

/*--------------------------------------------------------------------------
　　コーナー処理
--------------------------------------------------------------------------*/
$(function(){
	$("div#employ_contents").wrapInner("<div class='inner'></div>");
	$("div#employ_contents div.inner").after("<div class='corner_bottom'>　</div>");
	$("div#information").wrapInner("<div class='information_bottom'></div>");
});

/*--------------------------------------------------------------------------
　　ロールオーバー処理
--------------------------------------------------------------------------*/
$ (function () {
	$ ("img.hover").each (function () {
		var source = $ (this).css ("filter").indexOf ("progid") != -1 ? $ (this).css ("filter").split ("\"")[1] : this.src;
		$ ("<img>").attr ("src", source.replace (/\.([^.]+)$/, "_hover." + "$1")).appendTo ("body").hide ();
	});
	$ ("a:has(img.hover)").hover (function () {
		$ ("img.hover", this).each (function () {
			var source = $ (this).css ("filter").indexOf ("progid") != -1 ? $ (this).css ("filter").split ("\"")[1] : this.src;
			this.src = source.replace (/\.([^.]+)$/, "_hover." + "$1");
		});
	}, function () {
		$ ("img.hover", this).each (function () {
			var source = $ (this).css ("filter").indexOf ("progid") != -1 ? $ (this).css ("filter").split ("\"")[1] : this.src;
			this.src = source.replace (/_hover\.([^.]+)$/, "." + "$1");
		});
	});
	$ ("input.hover").each (function () {
		$ ("<img>").attr ("src", this.src.replace (/\.([^.]+)$/, "_hover." + "$1")).appendTo ("body").hide ();
		$ (this).hover (function () {
			this.src = this.src.replace (/\.([^.]+)$/, "_hover." + "$1");
		}, function () {
			this.src = this.src.replace (/_hover\.([^.]+)$/, "." + "$1");
		});
	});
});

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

$(function(){
	$("input[@type='text'],textarea")
		.focus(function(){
			$(this).addClass("focus");
		})
		
		.blur(function(){
			$(this).removeClass("focus");
		});
});

/*--------------------------------------------------------------------------
　　li要素のロールオーバー処理
--------------------------------------------------------------------------*/
$(function(){
	$("li").hover (
		function() {
			$(this).addClass("hover");
		},
		function() {
			$(this).removeClass("hover");
		}
	);
});

/*--------------------------------------------------------------------------
　　img要素のロールオーバー処理
--------------------------------------------------------------------------*/
$(function(){
	$("img").hover (
		function() {
			$(this).addClass("hover");
		},
		function() {
			$(this).removeClass("hover");
		}
	);
});
