$(function(){
	$('.contentsBox').tile();

	$('#staffCatNav dd a').on({
		'mouseenter': function() {
			$(this).not('.current').children('img').attr('src', $(this).children('img').attr('src').replace('_off', '_on'));
		},
		'mouseleave': function() {
			$(this).not('.current').children('img').attr('src', $(this).children('img').attr('src').replace('_on', '_off'));
		},
		'click': function() {
			var filter_cat = $(this).data('cat');
			$('#staffList li').removeClass('selectoff');
			if (!$(this).hasClass('current')) {
				$('#staffCatNav dd a').removeClass('current');
				$(this).addClass('current');
				$('#staffList li').not('.' + filter_cat).addClass('selectoff');
			} else {
				$(this).removeClass('current');
			}
			$.each($('#staffCatNav dd'), function(e) {
				if (!$('#staffCatNav dd:eq(' + e + ') a').hasClass('current')) {
					$('#staffCatNav dd:eq(' + e + ') img').attr('src', $('#staffCatNav dd:eq(' + e + ') img').attr('src').replace('_on', '_off'));
				}
			});

			return false;
		}
	});

	$('.comingsoon').on('click', function() {
		return false;
	});
});

$(window).load(function() {
	$('.staffWrap').tile(8);
});