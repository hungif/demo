<div class="asideBox asideListLink">
<p class="asideTTl"><img src="/common/image/shared/aside/ttl_asideQa.gif" alt="よくあるご質問" width="101" height="15"></p>
<ul>
<li><a href="/qa/06.html#q01">いつまでに出せばいいのですか？</a></li>
<li><a href="/qa/06.html#q02">スーツケースにカバーはかけなければいけないのですか？</a></li>
<li><a href="/qa/06.html#q03">空港宅急便は追加料金がかかりますか？</a></li>
<li><a href="/qa/06.html#q04">往復利用の際の割引はありますか？</a></li>
</ul>
<!-- /.asideBox --></div>
