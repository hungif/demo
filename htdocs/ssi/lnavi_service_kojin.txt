<div class="asideBox lNavTgBoxTSeK01">
<div class="lNavTtl"><a href="/c/index.html">商品・サービス（個人のお客様）</a></div>
<div class="lNavTgBoxB"><div class="lNavTgBoxM">
<div class="lNavTg"><p class="icoClose">国内へお届け</p></div>
<ul>
<li><a href="/takkyubin/takkyu.html">宅急便</a></li>
<li><a href="/compact/compact.html">宅急便コンパクト</a></li>
<li><a href="/cool/cool.html">クール宅急便</a></li>
<li><a href="/golf/golf.html">ゴルフ宅急便</a></li>
<li><a href="/ski/ski.html">スキー宅急便</a></li>
<li><a href="/kuukou/kuukou.html">空港宅急便</a></li>
<li><a href="/oofuku/oofuku.html">往復宅急便</a></li>
<li><a href="/pasotaku/pasotaku.html">パソコン宅急便</a></li>
<li><a href="/auction/auction.html">オークション宅急便</a></li>
<li><a href="/time/time.html">宅急便タイムサービス</a></li>
<li><a href="/chosoku/chosoku.html">超速宅急便</a></li>
<li><a href="/corect/corect.html">宅急便コレクト</a></li>
<li><a href="/nekoposu/nekoposu.html">ネコポス</a></li>
<li><a href="/services/tomeoki.html">営業所止置きサービス</a></li>
<li><a href="/takkyubin/waribiki.html">複数口減額制度・クロネコメンバー割・回数券</a></li>
<li><a href="/yamatobin/yamatobin.html">ヤマト便</a></li>
<li class="blankLink"><a href="http://www.008008.jp/transport/kazai/" target="_blank"><img src="/common/image/shared/parts/blank_link.gif" alt="新しいウィンドウが開きます" width="13" height="9">らくらく家財宅急便</a></li>
<li class="blankLink"><a href="http://www.y-logi.com/ylc/art/bijutubin.html" target="_blank"><img src="/common/image/shared/parts/blank_link.gif" alt="新しいウィンドウが開きます" width="13" height="9">美術便（国内・海外）サービス</a></li>
</ul>
<div class="lNavTg"><p class="icoClose">海外へお届け/海外からのお届け</p></div>
<ul>
<li><a href="/kokusaitakkyubin/kokusaitakkyubin.html">国際宅急便</a></li>
<li class="blankLink"><a href="http://www.y-logi.com/service/kaigai/service/ryutaku/index.html" target="_blank"><img src="/common/image/shared/parts/blank_link.gif" alt="新しいウィンドウが開きます" width="13" height="9">留学宅急便</a></li>
<li class="blankLink"><a href="http://www.y-logi.com/service/kaigai/service/rakuraku/index.html" target="_blank"><img src="/common/image/shared/parts/blank_link.gif" alt="新しいウィンドウが開きます" width="13" height="9">引越らくらく海外パック</a></li>
</ul>
<div class="lNavTg"><p class="icoClose">クロネコメンバーズのサービス</p></div>
<ul>
<li class="blankLink"><a href="/webservice_guide/shuka_off.html" target="_blank"><img src="/common/image/shared/parts/blank_link.gif" alt="新しいウィンドウが開きます" width="13" height="9">集荷受付</a></li>
<li class="blankLink"><a href="/webservice_guide/pc_off.html" target="_blank"><img src="/common/image/shared/parts/blank_link.gif" alt="新しいウィンドウが開きます" width="13" height="9">パソコン宅急便集荷受付</a></li>
<li class="blankLink"><a href="/webservice_guide/memberwari_off.html" target="_blank"><img src="/common/image/shared/parts/blank_link.gif" alt="新しいウィンドウが開きます" width="13" height="9">クロネコメンバー割</a></li>
<li><a href="/rakuraku/rakuraku.html">らくらく送り状発行サービス</a></li>
<li><a href="/takuhai/takuhai.html">宅配ロッカー発送サービス</a></li>
<li><a href="/c2/c2.html">送り状発行システムC2</a></li>
<li><a href="/yotei/yotei_hatsu.html">お届け予定eメール（お荷物を出される個人のお客さま向け）</a></li>
<li><a href="/okurijyo/okurijyo.html">宅急便送り状印字サービス</a></li>
<li><a href="/otodoke/otodoke_k.html">お届け完了eメール（個人のお客さま向け）</a></li>
<li><a href="/uketori/uketori.html">宅急便受取指定</a></li>
<li><a href="/nichijihenkou/nichijihenkou.html">「荷物お問い合わせシステム」からのお受け取り日時・場所変更</a></li>
<li class="blankLink"><a href="/webservice_guide/sai_off.html" target="_blank"><img src="/common/image/shared/parts/blank_link.gif" alt="新しいウィンドウが開きます" width="13" height="9">再配達受付</a></li>
<li><a href="/yotei/yotei_chaku.html">お届け予定eメール（お荷物を受け取られるお客さま向け）</a></li>
<li><a href="/tentou/tentou.html">宅急便店頭受取りサービス</a></li>
<li><a href="/gofuzai/gofuzai.html">ご不在連絡eメール</a></li>
<li><a href="/tenkyo/tenkyo.html">宅急便転居転送サービス</a></li>
<li><a href="/auction/auction.html">オークション宅急便</a></li>
<li class="blankLink"><a href="http://www.kuroneko-kadendr.jp/hosho/index.html" target="_blank"><img src="/common/image/shared/parts/blank_link.gif" alt="新しいウィンドウが開きます" width="13" height="9">クロネコ家電Dr.（修理・延長保証）</a></li>
</ul>
<div class="lNavTg"><p class="icoOpen">その他商品・サービス</p></div>
<ul>
<li><a href="/tokusen/tokusen.html">クロネコヤマトの得選市場</a></li>
<li><a href="/sizai/sizai.html">包装資材案内</a></li>
<li class="blankLink"><a href="http://www.y-logi.com/kuronekomuseum/" target="_blank"><img src="/common/image/shared/parts/blank_link.gif" alt="新しいウィンドウが開きます" width="13" height="9">インターネット美術館</a></li>
<li class="blankLink"><a href="http://www.y-logi.com/service/kaigai/index.html" target="_blank"><img src="/common/image/shared/parts/blank_link.gif" alt="新しいウィンドウが開きます" width="13" height="9">海外引越し</a></li>
<li class="blankLink"><a href="http://www.008008.jp/moving/" target="_blank"><img src="/common/image/shared/parts/blank_link.gif" alt="新しいウィンドウが開きます" width="13" height="9">国内引越し</a></li>
<li class="blankLink"><a href="http://www.kuronekofax.net/" target="_blank"><img src="/common/image/shared/parts/blank_link.gif" alt="新しいウィンドウが開きます" width="13" height="9">クロネコ@FAX</a></li>
</ul>
</div></div>
<!-- /.asideBox --></div>
<div class="asideBox mb10">
<a href="/b/index.html"><img src="/common/image/shared/aside/btn_service_off.png" width="228" height="57" alt="商品・サービス（法人・個人事業主のお客様）" class="png"></a>
<!-- /.asideBox --></div>

