$(document).ready(function(){
	var x = 800;

	$('.ui-close').click(function(){
		$('.modal').fadeOut();
	});
	$('.ui-next').click(function(){
		var $wrapper = $("#slides");
		var flag = $wrapper.attr("class");
		if(flag == 's1'){
			$wrapper.animate({"left": "-800px"});
			$wrapper.attr("class","s2");
		}else if(flag == 's2'){
			$wrapper.animate({"left": "-1600px"});
			$wrapper.attr("class","s3");
		}else if(flag == 's3'){
			$wrapper.animate({"left": "-2400px"});
			$wrapper.attr("class","s4");
		}else if(flag == 's4'){
			$wrapper.animate({"left": "-3200px"});
			$wrapper.attr("class","s5");
		}else if(flag == 's5'){
			$wrapper.animate({"left": "-4000px"});
			$wrapper.attr("class","s6");
		}else if(flag == 's6'){
			$wrapper.animate({"left": "-4800px"});
			$wrapper.attr("class","s7");
		}else if(flag == 's7'){
			$wrapper.animate({"left": "-5600px"});
			$wrapper.attr("class","s8");
		}else if(flag == 's8'){
			$wrapper.animate({"left": "-6400px"});
			$wrapper.attr("class","s9");
		}else if(flag == 's9'){
			$wrapper.animate({"left": "0px"});
			$wrapper.attr("class","s1");
		}
		if(flag == 's8'){
			$('.ui-next').hide();
		}else{
			$('.ui-next').show();
		}
		if(flag == 's1'){
			$('.ui-prev').show();
		}
	});
	$('.ui-prev').click(function(){
		var $wrapper = $("#slides");
		var flag = $wrapper.attr("class");
		if(flag == 's3'){
			$wrapper.animate({"left": "-800px"});
			$wrapper.attr("class","s2");
		}else if(flag == 's4'){
			$wrapper.animate({"left": "-1600px"});
			$wrapper.attr("class","s3");
		}else if(flag == 's5'){
			$wrapper.animate({"left": "-2400px"});
			$wrapper.attr("class","s4");
		}else if(flag == 's6'){
			$wrapper.animate({"left": "-3200px"});
			$wrapper.attr("class","s5");
		}else if(flag == 's7'){
			$wrapper.animate({"left": "-4000px"});
			$wrapper.attr("class","s6");
		}else if(flag == 's8'){
			$wrapper.animate({"left": "-4800px"});
			$wrapper.attr("class","s7");
		}else if(flag == 's9'){
			$wrapper.animate({"left": "-5600px"});
			$wrapper.attr("class","s8");
		}else if(flag == 's1'){
			$wrapper.animate({"left": "-6400px"});
			$wrapper.attr("class","s9");
		}else if(flag == 's2'){
			$wrapper.animate({"left": "0px"});
			$wrapper.attr("class","s1");
		}

		if(flag == 's2'){
			$('.ui-prev').hide();
		}else{
			$('.ui-prev').show();
		}

		if(flag == 's9'){
			$('.ui-next').show();
		}
	});
	$('.modal-triggers li').click(function(){
		$('.modal').fadeIn();
		var $wrapper = $("#slides");
		var flag = $(this).data("slide");
		if(flag == 's2'){
			$wrapper.animate({"left": "-800px"});
			$wrapper.attr("class","s2");
		}else if(flag == 's3'){
			$wrapper.animate({"left": "-1600px"});
			$wrapper.attr("class","s3");
		}else if(flag == 's4'){
			$wrapper.animate({"left": "-2400px"});
			$wrapper.attr("class","s4");
		}else if(flag == 's5'){
			$wrapper.animate({"left": "-3200px"});
			$wrapper.attr("class","s5");
		}else if(flag == 's6'){
			$wrapper.animate({"left": "-4000px"});
			$wrapper.attr("class","s6");
		}else if(flag == 's7'){
			$wrapper.animate({"left": "-4800px"});
			$wrapper.attr("class","s7");
		}else if(flag == 's8'){
			$wrapper.animate({"left": "-5600px"});
			$wrapper.attr("class","s8");
		}else if(flag == 's9'){
			$wrapper.animate({"left": "-6400px"});
			$wrapper.attr("class","s9");
		}else if(flag == 's1'){
			$wrapper.animate({"left": "0px"});
			$wrapper.attr("class","s1");
		}

		if(flag == 's1'){
			$('.ui-prev').hide();
			$('.ui-next').show();
		}else if(flag == 's9'){
			$('.ui-prev').show();
			$('.ui-next').hide();
		}else{
			$('.ui-prev').show();
			$('.ui-next').show();
		}

	});
	
  /* smooth scroll */
  $('a[href^=#]:not(.movieModal)').click(function() {
    // スクロールの速度
    var speed = 400; // ミリ秒
    // アンカーの値取得
    var href= $(this).attr("href");
    // 移動先を取得
    var target = $(href == "#" || href == "" ? 'html' : href);
    // 移動先を数値で取得
    var position = target.offset().top;
    // スムーススクロール
    $('body,html').animate({scrollTop:position}, speed, 'swing');
    return false;
  });	
	
});

