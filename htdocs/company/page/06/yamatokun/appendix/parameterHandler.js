// parameterHandler.js made by jjkim. at 2003.05.19
// Please do not input the 2bytes characters for foreign web-agent.

// To open with the "startpage" parameter, insert the below strings at the BODY onLoad tag.
// onLoad="setStartPage( getStartPage() );"

/***
	[How to set up for the startpage]
	1. default1.html : <script language="JavaScript" src="appendix/parameterHandler.js"></script>
	2. default1.html : <BODY onLoad="JavaScript:setStartPage( getStartPage() )">
***/


function getAllParameters()		// get params for external service
{
	return;						// this method has the problem, multi frames have multi params.
}


function getParameter( strParamName )				
{
	//  input : String, Parameter name from HTTP GET Paramemeter.
	// output : String, Parameter value which is parsed.
	
	var strParamValue = "[null]";
    var objWin = this;
    
    try 
    {
        while ( objWin != null )
        {
    		var strThisURL = objWin.location.href.toLowerCase();
    		
            if( -1 != strThisURL.indexOf( strParamName + "=",0 ) )
            {
        		var strParam = strThisURL.substring( strThisURL.indexOf(strParamName + "=",0), strThisURL.length );
        		if ( strParam == null || strParam == "" )		break;
        		var arrParam = strParam.split( "&" );
        		strParamValue = arrParam[0].substring( arrParam[0].indexOf(strParamName + "=",0)+strParamName.length+1, arrParam[0].length );
    	    }
      	    if ( objWin.parent==null || objWin==window.top ) 	break;
    	    else objWin = objWin.parent;
        }
    } 
    catch (e) 
    {
    	alert( "Parameter parsing failure.\nPlease check the frameset of HTML." );
    }
    
    return strParamValue;
}

//	for debug : alert( "Extracted Parameter Value = '" + getParameter( 'abc' ) + "'" );
//  USAGE : <script language="JavaScript" src="getParameter.js"></script>


function getStartPage()				
{
	var strStartPage = "0";			// default start page = 0
	
	strStartPage = getParameter( "startpage" );
    
    if ( isNaN( strStartPage ) )	strStartPage = "0";		// exception handler for non-digit type string.
    
    return strStartPage;
}

//	for debug : alert( "Extracted StartPage = '" + getStartPage() + "'" );



function setStartPage( strStartPage ) 
{	
	if ( strStartPage == null || strStartPage == "" )  
		strStartPage = "0";			// default start page = 0
	
	if ( document.dBook == null )	return;		// please insert ErrMsg?

	document.dBook.handle_inputGotoPage( strStartPage );
}


function getMailingParam()		// get params for 3DDM
{
	var objWin = this;
	var strThisHREF = "";
	
    while ( objWin != null )
    {
		strThisHREF = objWin.location.href;    	
		if( -1 != strThisHREF.indexOf("RcptEmail=",0) )		break;
  	    if ( objWin.parent==null || objWin==window.top ) 	break;
	    else objWin = objWin.parent;
    }
    
    return strThisHREF;
}
//	alert( "Extracted StartPage = '" + getStartPage() + "'" );
//	alert( "getMailingParam() = " + getMailingParam() );




