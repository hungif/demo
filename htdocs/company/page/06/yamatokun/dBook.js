////////////////////////////////////////////////////////////////////////////////
function HD_pressCloseUp() {		
	document.dBook.handle_pressCloseUp();
}
function HD_pressZoom() {			
	document.dBook.handle_pressZoom();
}
function HD_pressLeftZoom() {		
	document.dBook.handle_pressZoomPage(document.dBook.handle_getLeftSlidePage());
}
function HD_pressRightZoom() {			
	document.dBook.handle_pressZoomPage(document.dBook.handle_getRightSlidePage());
}
function HD_pressPause() {			
	document.dBook.handle_pressPause();
}
function HD_pressNextPage() {		
	document.dBook.handle_pressNextPage();
}
function HD_pressPreviousPage() {	
	document.dBook.handle_pressPreviousPage();
}
function HD_gotoFirstPage() {		
	document.dBook.handle_gotoFirstPage();
}
function HD_gotoEndPage() {			
	document.dBook.handle_gotoEndPage();
}

function HD_pressBackwardRotate() {	// auto sliding (backward)
	document.dBook.handle_pressBackwardRotate();
}
function HD_pressForwardRotate() {	// auto sliding (forward)
	document.dBook.handle_pressForwardRotate();
}
function HD_pressSpeedUp() {		// turn speed up when sliding
	document.dBook.handle_pressSpeedUp();
}
function HD_pressSpeedDown() {		// turn speed down when sliding
	document.dBook.handle_pressSpeedDown();
}

function DigitalBookUnload()
{ 
	document.dBook.handle_setURL( getMailingParam() );
	document.dBook.handle_unload();
}
// Please append 'onUnload=DigitalBookUnload()' at BODY open tag.
// Please check that 'Port' number is 80 or not.
////////////////////////////////////////////////////////////////////////////////

function HD_openBrWindow(theURL,winName,features) {
		
	if (features == "")
	{
		features = "toolbar=yes,width=800,height=600,location=yes,directories=no,status=yes,menubar=yes,scrollbars=yes,resizable=yes";
	}

  HD_popup = window.open(theURL,winName,features);
  
  if (HD_popup != null)
  {
	  HD_popup.focus();
	}
}

var printTarget = ""

function HD_setPrintTarget(tempTarget) {
	printTarget = tempTarget
}

function printPage(direction) 
{
	var strMediaURL = document.dBook.handle_getMediaURL();
	
	if (strMediaURL != "")
	{
	
		if (document.dBook.handle_isDoublePage())
		{
			var	lpage = document.dBook.handle_getLeftSlidePage() - 1;
			var rpage = document.dBook.handle_getRightSlidePage() - 1;
			
			var page;
				
			if (direction == 0)
				page = 	lpage;
			else if (direction == 1)
				page = 	rpage;
			else
				page = 	lpage + "-" + rpage;	
		}
		else
		{
			var page = document.dBook.handle_getCurSlidePage() - 1;
		}
		
		PrintCtrl.PrintHDM("\"" + strMediaURL + "\"", -1, page);
		
		return;
	}


	var validity;
	
	var strPrintImages;
	
	if (document.dBook.handle_isDoublePage())
	{
		if (direction != 2) {		
			if (direction == 0)
				validity = document.dBook.handle_getLeftPrintName()
			else
				validity = document.dBook.handle_getRightPrintName()
			
			if (validity > 0) {
				strPrintImages = "\"" + printTarget + "0.jpg\"" + " " + "\"" + printTarget + "1.jpg\"" + " " + "\"" + printTarget + "2.jpg\"" + " " + "\"" + printTarget + "3.jpg\"";

			}
			else {
				strPrintImages = "\"" + printTarget + ".jpg\""
			}
		}
		else { //  (direction == 2) {		
			validity = document.dBook.handle_getLeftPrintName()
			var leftPage = printTarget;
			validity = document.dBook.handle_getRightPrintName()
			var RightPage = printTarget;

			if (validity > 0) {
				strPrintImages = "\"" + leftPage + "0.jpg\"" + " " + "\"" + leftPage + "1.jpg\"" + " " + "\"" + leftPage + "2.jpg\"" + " " + "\"" + leftPage + "3.jpg\"" + " " + "\"" + RightPage + "0.jpg\"" + " " + "\"" + RightPage + "1.jpg\"" + " " + "\"" + RightPage + "2.jpg\"" + " " + "\"" + RightPage + "3.jpg\"";
			}
			else {
				strPrintImages = "\"" + leftPage + ".jpg\"" + " " + "\"" + RightPage + ".jpg\""
			}
		}
	}
	else
	{
		validity = document.dBook.handle_getCurPrintName();
		
		if (validity > 0) {
			strPrintImages = "\"" + printTarget + "0.jpg\"" + " " + "\"" + printTarget + "1.jpg\"" + " " + "\"" + printTarget + "2.jpg\"" + " " + "\"" + printTarget + "3.jpg\"";
		}
		else {
			strPrintImages = "\"" + printTarget + ".jpg\""
		}
	}

	PrintCtrl.PrintImage(strPrintImages);
}

function printPage0(direction) {
	var tempFile=""
	var tempWindow = ""
	var validity

	if (direction != 2) {		
		if (direction == 0)
			validity = document.dBook.handle_getLeftPrintName()
		else
			validity = document.dBook.handle_getRightPrintName()

		printWindow=window.open(tempFile,tempWindow,'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,left=0, top=0, width=700,height=600');
		printWindow.document.writeln("<HTML>")
		printWindow.document.writeln("<head>")
		printWindow.document.writeln("<scr"+"ipt lang" + "uage=jav"+"ascript>")
		printWindow.document.writeln("    funct" + "ion printPage() {")
		
		printWindow.document.writeln("    window.print()")
				
		printWindow.document.writeln("		setTimeout('window.close()', 1000);")
		printWindow.document.writeln("    } ")
		printWindow.document.writeln("</scr"+"ipt>")
		printWindow.document.writeln("</head>")
		printWindow.document.writeln("<BODY>")

		printWindow.document.writeln("<center>")
		printWindow.document.writeln("<table border='0' cellspacing='0' cellpadding='0' topmargin='0' marginwidth='0' marginheight='0'>")
		printWindow.document.writeln("<tr>")

		if (validity > 0) {
			printWindow.document.writeln("<td><img src=\"" + printTarget + "0.jpg\" width=325 height=485></td>")
			printWindow.document.writeln("<td><img src=\"" + printTarget + "1.jpg\" width=325 height=485></td>")
			printWindow.document.writeln("</tr><tr>")
			printWindow.document.writeln("<td><img src=\"" + printTarget + "2.jpg\" width=325 height=485></td>")
			printWindow.document.writeln("<td><img src=\"" + printTarget + "3.jpg\" width=325 height=485></td>")
		}
		else {
			printWindow.document.writeln("<td><img src=\"" + printTarget + ".jpg\" width=650 height=970></td>")
		}

		printWindow.document.writeln("</tr>")
		printWindow.document.writeln("</table>")
		printWindow.document.writeln("</center>")

		printWindow.document.writeln("</BODY>")
		printWindow.document.writeln("</HTML>")
		printWindow.location.reload();

//		printWindow.print()
		printWindow.setTimeout("printPage()", 1000);
		printWindow.setTimeout("window.close()",1000);
	}
	else { //  (direction == 2) {		
		validity = document.dBook.handle_getLeftPrintName()
		var leftPage = printTarget;
		validity = document.dBook.handle_getRightPrintName()
		var RightPage = printTarget;

		printWindow=window.open(tempFile,tempWindow,'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes, left=0, top=0, width=910,height=700');
		printWindow.document.writeln("<HTML>")
		printWindow.document.writeln("<head>")

		printWindow.document.writeln("<scr"+"ipt lang" + "uage=jav"+"ascript>")
		printWindow.document.writeln("		var msg = 'Set the page orientaion of the printer to the landscape.'")
		printWindow.document.writeln("    funct" + "ion printPage() {")
		
		printWindow.document.writeln("    window.print()")
		
		printWindow.document.writeln("				setTimeout('alert(msg)', 1500)")
		printWindow.document.writeln("				setTimeout('window.close()', 2000);")
		printWindow.document.writeln("    } ")
		printWindow.document.writeln("</scr"+"ipt>")

		printWindow.document.writeln("</head>")
		printWindow.document.writeln("<BODY>")

		printWindow.document.writeln("<center>")
		printWindow.document.writeln("<table border='0' cellspacing='0' cellpadding='0' topmargin='0' marginwidth='0' marginheight='0'>")
		printWindow.document.writeln("<tr>")

		if (validity > 0) {
			printWindow.document.writeln("<td><img src=\"" + leftPage + "0.jpg\" width=225 height=322></td>")
			printWindow.document.writeln("<td><img src=\"" + leftPage + "1.jpg\" width=225 height=322></td>")
			printWindow.document.writeln("<td><img src=\"" + RightPage + "0.jpg\" width=225 height=322></td>")
			printWindow.document.writeln("<td><img src=\"" + RightPage + "1.jpg\" width=225 height=322></td>")
			printWindow.document.writeln("</tr><tr>")
			printWindow.document.writeln("<td><img src=\"" + leftPage + "2.jpg\" width=225 height=322></td>")
			printWindow.document.writeln("<td><img src=\"" + leftPage + "3.jpg\" width=225 height=322></td>")
			printWindow.document.writeln("<td><img src=\"" + RightPage + "2.jpg\" width=225 height=322></td>")
			printWindow.document.writeln("<td><img src=\"" + RightPage + "3.jpg\" width=225 height=322></td>")
		}
		else {
			printWindow.document.writeln("<td><img src=\"" + leftPage + ".jpg\" width=450 height=645></td>")
			printWindow.document.writeln("<td><img src=\"" + RightPage + ".jpg\" width=450 height=645></td>")
		}

		printWindow.document.writeln("</tr>")
		printWindow.document.writeln("</table>")
		printWindow.document.writeln("</center>")

		printWindow.document.writeln("</BODY>")
		printWindow.document.writeln("</HTML>")
		printWindow.location.reload();

		printWindow.setTimeout("printPage()", 1000);
	}
}

//function HD_startApplet() {
//	interval_id = window.setInterval("HD_setPageNum()", intervalTime);
//}

////////////////////////////////////////////////////////////////////////////////
