<div id="globalNav">
<ul id="nav" class="gnavPulldown">
<li id="gNavBt01"><a href="/en/personal/" class="gnavarea">Personal</a>
<ul class="gNavList01">
<li><a href="/en/personal/">Japan Delivery</a></li>
<li><a href="/en/personal/international/">International Delivery</a></li>
<li><a href="/en/personal/others/">Other Services</a></li>
</ul></li>

<li id="gNavBt02"><a href="/en/send/" class="gnavarea">Sending</a>
<ul class="gNavList02">
<li><a href="/en/send/domestic/">Sending in Japan</a></li>
<li><a href="/en/send/international/">Sending Overseas</a></li>
<li><a href="/en/send/#link01">Preparing for Shipping</a></li>
</ul></li>

<li id="gNavBt03"><a href="/en/receive/" class="gnavarea">Receiving</a>
<ul class="gNavList02">
<li><a href="/en/receive/domestic/area/">Receiving in Japan</a></li>
<li><a href="/en/receive/international/contact/">Receiving Overseas</a></li>
<li><a href="/en/receive/redeliver/howto/">Re-delivery</a></li>
</ul></li>

<li id="gNavBt04"><a href="/en/business/" class="gnavarea">Business</a>
<ul class="gNavList02">
<li><a href="/en/business/">Japan Delivery</a></li>
<li><a href="/en/business/international/">International Delivery</a></li>
<li><a href="/en/business/solution/">Solutions</a></li>
</ul></li>

<li id="gNavBt05"><a href="/en/corporate/" class="gnavarea">About Us</a>
<ul class="gNavList02">
<li><a href="/en/corporate/terms/">Company Information</a></li>
</ul></li>

<li id="gNavBt06"><a href="/en/contact/" class="gnavarea">Inquiry</a>
<ul class="gNavList03">
<li><a href="/en/contact/faq/">FAQs</a></li>
</ul></li>
<!-- /#nav .gnavPulldown --></ul>
<!-- /#globalNav --></div>
