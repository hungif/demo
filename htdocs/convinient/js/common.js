//ユーザエージェント条件分岐
/*var _ua = (function(u){
	return {
		ltIE6:typeof window.addEventListener == "undefined" && typeof document.documentElement.style.maxHeight == "undefined",
		ltIE7:typeof window.addEventListener == "undefined" && typeof document.querySelectorAll == "undefined",
		ltIE8:typeof window.addEventListener == "undefined" && typeof document.getElementsByClassName == "undefined",
		ltIE9:document.uniqueID && typeof window.matchMedia == "undefined",
		Tablet:(u.indexOf("windows") != -1 && u.indexOf("touch") != -1)
		|| u.indexOf("ipad") != -1
		|| (u.indexOf("android") != -1 && u.indexOf("mobile") == -1)
		|| (u.indexOf("firefox") != -1 && u.indexOf("tablet") != -1)
		|| u.indexOf("kindle") != -1
		|| u.indexOf("silk") != -1
		|| u.indexOf("playbook") != -1,
		Mobile:(u.indexOf("windows") != -1 && u.indexOf("phone") != -1)
		|| u.indexOf("iphone") != -1
		|| u.indexOf("ipod") != -1
		|| (u.indexOf("android") != -1 && u.indexOf("mobile") != -1)
		|| (u.indexOf("firefox") != -1 && u.indexOf("mobile") != -1)
		|| u.indexOf("blackberry") != -1
	}
})(window.navigator.userAgent.toLowerCase());*/

//ロールオーバー関数（同じ画像ファイル名+_onをロールオーバー画像にする）
$.fn.rollover = function(){
	return this.each(function(){
		var _on = $(this).attr('src').replace(/\.\w+$/, '_on$&');
		var src = $(this).attr('src');
		$('<img>').attr('src', _on);
		$(this).hover(function(){
			$(this).attr('src', _on);
		},function(){
			$(this).attr('src', src);
		});
	});
};

$(function() {



	/*if(_ua.Mobile){
		console.log('スマホの処理');
	}else if(_ua.Tablet){
		console.log('タブレットの処理');
	}else if(_ua.ltIE6){
		console.log('IE6以下の処理');
	}else if(_ua.ltIE7){
		console.log('IE7以下の処理');
	}else if(_ua.ltIE8){
		console.log('IE8以下の処理');
	}else if(_ua.ltIE9){
		console.log('IE9の処理');
	}else {
		console.log('それ以外の処理');
	}*/

	//ロールオーバー処理（引数指定で『_on』の部分を変更可能）

	//ロールオーバーに画像を使わず透明度で処理
	$('a img').hover(function(){
		$(this).stop().fadeTo(0,0.75);	
	},function(){
		$(this).stop().fadeTo(0,1.00);	
	});

	//スムーススクロール
	$('a[href^=#]').on('click',function() {
		var speed = 500;
		var href = $(this).attr('href');							//アンカーの値取得
		var target = $(href == '#' || href == '' ? 'html' : href);	//移動先を取得
		var position = target.offset().top;							//移動先を数値で取得
		$('html, body').animate({scrollTop:position}, speed, 'swing');
		return false;
	});





});
