
var num = 0;
var heightFlag = false;
var currentFsize = 0;


var observe = window.addEventListener
? function(target, type, listener) { 
	if(target != null) {
		 target.addEventListener(type, listener, false); 
	}
}
: function(target, type, listener) { 
	if(target != null) {
		target.attachEvent('on' + type, function() { listener.call(target, window.event); }); 
	}
};
	
var deleteObserve = window.addEventListener
? function(target, type, listener) { target.removeEventListener(type, listener, false); }
: function(target, type, listener) { target.detachEvent('on' + type, function() { listener.call(target, window.event); }); }; 


function Btn(elm) {
	var me = this;
	this.elm = elm;
	this.over = function() { me.changeImage(true); };
	this.out = function() {	me.changeImage(false); };
	this.init();
}

var _Btn = Btn.prototype;

_Btn.init = function() {
	var me = this;
	var img_on = new Image();
	var img_str = this.elm.getAttribute("src");
	img_on.src = img_str.replace("_off", "_on");
	this.setEvent();
};


_Btn.doSelect = function() {
	this.deleteEvent();
	this.changeImage(true);
};


_Btn.setEvent = function() {
	try {
		this.elm.addEventListener("mouseover", this.over, false);
		this.elm.addEventListener("mouseout", this.out, false);
	} catch(e) {
		this.elm.attachEvent("onmouseover", this.over);
		this.elm.attachEvent("onmouseout", this.out);
	}
};


_Btn.deleteEvent = function() {
	try {
		this.elm.removeEventListener("mouseover", this.over, false);
		this.elm.removeEventListener("mouseout", this.out, false);
	} catch(e) {
		this.elm.detachEvent("onmouseover", this.over);
		this.elm.detachEvent("onmouseout", this.out);
	}
};


_Btn.changeImage = function(flag) {
	var img = this.elm.getAttribute("src");
	this.elm.setAttribute("src", (flag) ? img.replace("_off", "_on") : img.replace("_on", "_off"));
};



function dispH470(url){
	window.open(url, "window_name", "width=900,height=470,scrollbars=yes");
}

function dispW1000(url){
	window.open(url, "window_name", "width=1000,height=500,scrollbars=yes");
}




function preload() {
 var img = new Image();
 img.src = "/common/image/shared/portal/img_slidebanner.png";
}



var TextInput = function(id, defaultText){
	this.elm = document.getElementById(id);
	this.defaultText = defaultText;
	this.init();
}

TextInput.prototype = {
	init : function(){
		var elm = this.elm;
		var defaultText = this.defaultText;
		observe(elm,"focus",function(){
			if(elm.value === defaultText) elm.value ="";
		});	
		observe(elm,"blur",function(){
			if(elm.value === "") {
				elm.value = defaultText;
			}
			else{
				deleteObserve(elm,"focus", arguments.callee);
				deleteObserve(elm,"blur", arguments.callee);
			}
		});	
	}
}


function saveCookie(key, value, days) {
	var str = key + "=" + escape(value) + ";";
	if (days != 0) {
		var dt = new Date(); 
		dt.setDate(dt.getDate() + days);
		str += "expires=" + dt.toGMTString() + ";";
	}
	document.cookie = str; 
}


function loadCookie(key) {
	var sCookie = document.cookie; 
	var aData = sCookie.split(";");
	var oExp = new RegExp(" ", "g"); 
	key = key.replace(oExp, ""); 
	var i = 0;
	while (aData[i]) {
		var aWord = aData[i].split("="); 
		aWord[0] = aWord[0].replace(oExp, ""); 
		if (key == aWord[0]) return unescape(aWord[1]); 
		if (++i >= aData.length) break;
	}
 return ""; 
}


function changeFont(val){
	document.getElementsByTagName('body')[0].className='changeFontSize'+val;
	setCookieFont(val);
	var btnNum = val;
	if(document.getElementById('fontSize') != null){
		var btnLength= document.getElementById('fontSize').getElementsByTagName('li').length; 
		for(var i = 1;i<=btnLength;i++)
		{
			if(btnNum == i){
				var img =document.getElementById('fontSize'+i);
				var imgPass = img.getAttribute("src");
				var onoff = "";
				if (imgPass.indexOf("_off") >= 0) onoff = "_off";
				if (imgPass.indexOf("_on") >= 0) onoff = "_on";
				if(onoff != "")
				{
					img.setAttribute('src',imgPass.replace(onoff, '_a'));  
				}
			}
			else{
				var img =document.getElementById('fontSize'+i);
				var imgPass = img.getAttribute('src');
				img.setAttribute('src',imgPass.replace('_a', '_off')); 
			}
		}
	}
}

function setCookieFont(val) {	
	cName = "NAME=";
	cDays = 30; 
	var setPre;
	setPre = new Date();
	setPre.setTime(setPre.getTime() + (cDays*1000*24*3600));
	prd = setPre.toGMTString();
	currentFsize = val;
	document.cookie = cName + val  + "; path=/; expires=" + prd;
}

function getCookieFont() {
	cData = "";
	cName = "NAME=";
	cCookie = document.cookie+";";
	str = cCookie.indexOf(cName);
	if(str != -1) {
		end = cCookie.indexOf(";",str);
		cData = unescape(cCookie.substring(str + cName.length, end));
		return cData;
 } else {
		return null;
	}
}

function setFontInit(){
	var cookiefont = getCookieFont();
	if (cookiefont != null) {
		currentFsize = cookiefont;
		changeFont(cookiefont);
	}else{
		currentFsize = 2;
		changeFont(2);
	}
}

function MM_jumpMenu(targ,selObj,restore){ //v3.0
  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}


/*-----------------------------
jQuery 
--------------------------------*/

if(typeof jQuery != "undefined"){ 
(function($) {


	$.easing.quart = function (x, t, b, c, d) {
		return -c * ((t=t/d-1)*t*t*t - 1) + b;
	};
	$(function() {  
		
		
		$(function() {
			if(navigator.userAgent.indexOf("MSIE 6.0") != -1 || navigator.userAgent.indexOf("MSIE 7.0") != -1) {
					var gnavHoverFlg = false;
				$("#gNavBt01,#gNavBt02,#gNavBt03,#gNavBt04,#gNavBt05,#gNavBt06").hover(
						function () {
							gnavHoverFlg = true;
							$(this).children("ul").addClass("active");
							$(this).addClass("gNavOver");
							//document.getElementById("searchSiteTxt").blur();
							$("#container").css("position","relative");
       $("#aside,#container").css("zIndex",-1);
						},
						function () {
							gnavHoverFlg = false;
								$(this).children("ul").removeClass("active");
							$(this).removeClass("gNavOver");
        $("#aside,#container").css("zIndex",0);
						}
				 );
				$("#searchSiteTxt").bind('focus',function(){
				
				  $("#container").css("position","relative");
      $("#aside,#container").css("zIndex",-1);
     			
					$("#searchbox").bind("mouseleave",function(){ 
					//		document.getElementById("searchSiteTxt").blur();
					//	 $("#searchbox").unbind("mouseleave",arguments.callee);	
						});
				$("#searchbox").bind("click",function(){ 
					//document.getElementById("searchSiteTxt").blur();
						});
				});
				$("#searchSiteTxt").bind('blur',function(){
				
       if(!gnavHoverFlg) $("#aside,#container").css("zIndex",0);					
					});
				
				$("#searchSiteTxt").bind('propertychange',function(e) {
					e.preventDefault();
					var disp = $("#searchSiteTxt").css("display");
					if(disp == 'none') {
						 $("#aside,#container").css("zIndex",0);
					} else {
						 $("#container").css("position","relative");
						 $("#aside,#container").css("zIndex",-1);
					}
			 });
			}
		});
		
		

		$('a[href*=#]').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var jQuerytarget = jQuery(this.hash);
				jQuerytarget = jQuerytarget.length && jQuerytarget || jQuery('[name=' + this.hash.slice(1) +']');
				if (jQuerytarget.length) {
					var targetOffset = jQuerytarget.offset().top;
					jQuery('html,body').animate({ scrollTop: targetOffset }, 600, 'quart');
					return false;
				}
			}
		});

		var url = "";
		var flag = false;
		var params = [];
		var param = "";
		var myStr = param;
		
		if(location.href.indexOf("?") != -1) params = location.href.split("?");
		param = params[1];
		
		$(".tabHead li").each(function(){
			
			if($(this).attr("id") == "")
			{	
				if($(this).children("a").attr("href") == undefined)
				{
					num = $(this).index();
				}
				else
				{
					url = $(this).children("a").attr("href");
					var txt = $(this).children("a").text();
					$(this).html(txt);
				}
				flag = true;
			}
			else
			{
				if(param == $(this).attr("id"))
				{
					var ids = $(this).attr("id").split("b");
					num = (ids[1] != "0")? parseInt(ids[1]) - 1:0;
				}
			}			
		});
		
		$(".tabHead").each(function(){
			$(this).children("li").css("background-position","top left");
			$(this).children("li").css("color","#333");
			$(this).children("li").eq(num).addClass("tabCurrent");
			$(this).children("li").eq(num).css("background-position","top right");
			$(this).children("li").eq(num).css("color","#fff");
			$(this).children("li").eq(3).css("margin-right","0px");
		});
		
		$(".tabInner").each(function(){
			if(!flag)
			{
				//$(this).children("div").hide();
				//$(this).children("div").css("display","none");
				setTimeout( function() {
					
					if(num != 0)
					{
						$(".tabInner").children("div").eq(0).css("display","none");
						$(".tabInner").children("div").eq(num).css("display","block");
					}
					
					if(params.length >= 3 && location.href.indexOf("#") == -1) location.href = "#"+params[2];
				}, 10);	
				//alert($(".tabInner").height());
			}
		});
		
		$(".tabHead li").hover(function(){
			$(this).not(".tabCurrent").css("background-position","top right");
			$(this).not(".tabCurrent").css("color","#fff");
		},function(){
			$(this).not(".tabCurrent").css("background-position","top left");
			$(this).not(".tabCurrent").css("color","#333");
		});
		
		var tabClickEvent = function(){
			if($(this).attr("class")!="tabCurrent"){
				
				var cookiefont = getCookieFont();
				if (cookiefont == null) cookiefont = 2;
				
				heightFlag = true;
				changeFont(cookiefont);
				
				myStr = $(this).attr("id");
				
				if(myStr != "")
				{
					var nextTab = '#'+myStr+'Body';
					var curentStr = $(this).parent().children("li.tabCurrent").attr("id");
					var currentTab = '#'+curentStr+'Body';
					
					$(currentTab).parent().css("height",$(currentTab).height())
					
					$(currentTab).hide();
					$(nextTab).fadeIn("normal",function()
					{
						$(currentTab).parent().css("height","auto");
					});
					//location.href = "#"+myStr;
					
					$(this).parent().children("li.tabCurrent").css("background-position","top left");
					$(this).parent().children("li.tabCurrent").css("color","#333");
					$(this).parent().children("li.tabCurrent").removeClass("tabCurrent");
					$(this).addClass("tabCurrent");
					
				}
				else
				{
					location.href = url;
				}
			}
		}
		
		var names = [];
		var anc = "";
		
		var linkClickEvent = function(){
			
			if($(this).attr("name").indexOf("#") != -1)
			{
				names = $(this).attr("name").split("#");
				
				//if(anc ==  names[1]) return false;

				myStr = names[0];
				anc = names[1];
			}
			else
			{
				//alert(myStr);
				if(myStr == $(this).attr("name")) return false;
				myStr = $(this).attr("name");
			}
			
			var nextTab = '#'+myStr+'Body';
			$(".tabInner").children("div").hide();
			$(nextTab).fadeIn();
			
			$(".tabHead").children("li.tabCurrent").css("background-position","top left");
			$(".tabHead").children("li.tabCurrent").css("color","#333");
			$(".tabHead").children("li.tabCurrent").removeClass("tabCurrent");
			$("#"+myStr).addClass("tabCurrent");
			$("#"+myStr).css("background-position","top right");
			$("#"+myStr).css("color","#fff");
			
			if(anc != "")
			{
				location.href = "#" + anc;
			}
		}
		
		$(".tabHead li").click(tabClickEvent);
		$(".tabLink").click(linkClickEvent);
		

		$(".lNavTg").next("ul").hide();
		$(".lNavTg").children("p").removeClass("icoClose").addClass("icoOpen");
		$(".lNavTg").next("ul").children("li").each(function(){
		
		var preHref01=location.pathname.split('/')[2];
		var preHref02=location.pathname.split('/')[3];
		var mainhref=location.pathname.split('/')[4];

		if(mainhref=="index.html" || mainhref=="index2.html" || mainhref=="index3.html" || mainhref=="index4.html" || mainhref=="index5.html" ){
			var href = preHref01+preHref02;
		}
		else{
			var href = preHref01+preHref02+mainhref;
		}
		/* makeshift */
		if(href.match ('personalexpress')){	
		 var href = 'personalexpress';
		}			
		var pre01 = $(this).children("a").get(0).getAttribute('href',2).split('/')[2];
		var pre02 = $(this).children("a").get(0).getAttribute('href',2).split('/')[3];
		var mainPath = $(this).children("a").get(0).getAttribute('href',2).split('/')[4];
		var myPath =pre01+pre02+mainPath;
		
		
/*
			var href=location.pathname.split('/')[3];
			if($(this).children("a").get(0).getAttribute('href',2).split('/')[3] == href){
*/
			if(myPath == href){			
				$(this).parent().show();
				$(this).parent().prev().children("p").removeClass("icoOpen").addClass("icoClose");
				$(this).children("a").css("background-color","#FDE998").css("color","#003399");
				return false;
			}
		});
		
		$(".lNavTg").hover(function(){
			$(this).css("cursor","pointer");
			$(this).children("p").css("text-decoration","underline");
		},function(){
			$(this).css("cursor","default");
			$(this).children("p").css("text-decoration","none");
		});
		$(".lNavTg").click(function(){
			if($(this).children("p").attr("class")=="icoClose"){
				$(this).children("p").removeClass("icoClose").addClass("icoOpen");
			}else{
				$(this).children("p").removeClass("icoOpen").addClass("icoClose");
			}
			$(this).next("ul").slideToggle();
			changeFont(currentFsize);
		});
		$(".lNavCap").each(function(){
		var href=document.location.protocol + '//'+ document.location.hostname+ document.location.pathname;
			
		if($(this).children("a").get(0).href == href){
				$(this).children("a").css("background-color","#FDE998").css("color","#003399");
				$(this).children("a").css("border-bottom","#CCC solid 1px").css("color","#003399");
				return false;
			}
		});
		$("#lNaviCtr").children("li").each(function(){
		var href=document.location.protocol + '//'+ document.location.hostname+ document.location.pathname;
			
		if($(this).children("a").get(0).href == href){
				$(this).children("a").css("background-color","#FDE998").css("color","#003399");
				return false;
			}
		});
	});
	
/* IMGover */
	var btnArray01 = document.getElementsByTagName("img");
	var btnArray02 = document.getElementsByTagName("input");
	for (var i = 0, ln = btnArray01.length; i < ln; i++) {
		if (btnArray01[i].getAttribute("src").indexOf("_off.") >= 0) new Btn(btnArray01[i]);
	}
	for (var i = 0, ln = btnArray02.length; i < ln; i++) {
		try{
			if (btnArray02[i].getAttribute("src").indexOf("_off.") >= 0) new Btn(btnArray02[i]);
		} catch(e){
		}
	}	
	
	
})(jQuery);
}//end if jQuery
